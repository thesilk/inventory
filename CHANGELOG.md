# CHANGELOG

[unreleased]
## 0.3.0 (soon)
### Server
* move repository interfaces in own package

### CLI
* implement `show-inventory` and `update-inventory` subcommand
* add version flag
* use config file to manage server config access
* add `set-config` and `get-config` subcommand
* add `create-user`, `delete-user`, `update-user` and `show-users`
* update user should work like a patch

## 0.2.0 (23.12.2020)
### Server
* build production image locally and in CI -> `docker pull registry.gitlab.com/thesilk/inventory/prod.amd64:latest`
* add `longTermUsage` flag so if it's set to true the user won't be reset each night
* add api blueprint documentation
* update README.md
* delete go fmt in Makefile
* fix production image (use debian:buster-slim because of missing gcc in minideb)
* update build image
* install api blueprint on gitlab pages (https://thesilk.gitlab.io/inventory)
