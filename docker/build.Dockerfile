FROM node:lts-buster-slim
MAINTAINER Christian Seidemann <thesilk@posteo.net>

RUN apt-get update && apt-get install -y ca-certificates curl make git gcc

# Install golang 1.15.5
RUN curl -O https://dl.google.com/go/go1.15.5.linux-amd64.tar.gz && \
    echo -n "9a58494e8da722c3aef248c9227b0e9c528c7318309827780f16220998180a0d *go1.15.5.linux-amd64.tar.gz" | sha256sum -c && \
    tar xf go1.15.5.linux-amd64.tar.gz && \
    mv go /usr/local/ && \
    mkdir go

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.33.0

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/go/bin:/go/bin
ENV GOPATH=/go

RUN apt-get autoremove -y && apt-get clean

RUN npm install -g --unsafe-perm aglio
