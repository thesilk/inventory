FROM debian:buster-slim
MAINTAINER TheSilk

COPY inventory*.deb /tmp/inventory.deb
RUN dpkg -i /tmp/inventory.deb && rm /tmp/inventory.deb

ENTRYPOINT ["/usr/local/bin/inventory"]
EXPOSE 2342/tcp
