VERSION=$(shell git describe --abbrev=4 --dirty --always --tags)
DEB_VERSION=$(shell echo ${VERSION} | cut -c 2-)

UID=$(shell id -u)
GID=$(shell id -g)

GITLAB_REGISTRY=registry.gitlab.com
GITLAB_PROJECT_BUILD=thesilk/inventory/build:v1
GITLAB_PROJECT_PROD_AMD64=thesilk/inventory/prod.amd64:latest

LDFLAGS=-ldflags "-w -s -X main.version=${VERSION}"

APP_DEB_PACKAGE_SERVER := inventory-${VERSION}.deb
APP_DEB_PACKAGE_CLI := inventory-cli-${VERSION}.deb

${APP_DEB_PACKAGE_SERVER}: debian/*
	@rm -rf out/server
	@mkdir -p out/server/package/opt/inventory
	@mkdir -p out/server/package/var/opt/inventory
	@mkdir -p out/server/package/usr/local/bin
	@cp inventory out/server/package/opt/inventory/
	@cp debian/server/inventory.sh out/server/package/usr/local/bin/inventory
	@cp -r app/server/migrations out/server/package/opt/inventory/
	@mkdir -p out/server/package/DEBIAN
	@cp debian/server/control out/server/package/DEBIAN/
	@cp debian/server/postinst out/server/package/DEBIAN/
	@sed -i s/@VERSION@/${DEB_VERSION}/g out/server/package/DEBIAN/control
	@chmod -R 0775 out/server/package/DEBIAN
	@find ./debian -type d | xargs chmod 777
	@dpkg-deb -b out/server/package ${APP_DEB_PACKAGE_SERVER}

${APP_DEB_PACKAGE_CLI}: debian/*
	@rm -rf out/cli
	@mkdir -p out/cli/package/usr/local/bin
	@cp inventory-cli out/cli/package/usr/local/bin/
	@mkdir -p out/cli/package/DEBIAN
	@cp debian/cli/control out/cli/package/DEBIAN/
	@cp debian/cli/postinst out/cli/package/DEBIAN/
	@sed -i s/@VERSION@/${DEB_VERSION}/g out/cli/package/DEBIAN/control
	@chmod -R 0775 out/cli/package/DEBIAN
	@find ./debian -type d | xargs chmod 777
	@dpkg-deb -b out/cli/package ${APP_DEB_PACKAGE_CLI}

DOCKER_PARAM=-it --rm\
	 -v ${PWD}:/app\
	 -v ${GOPATH}/pkg/mod/cache:/go/pkg/mod/cache\
	 --env UID=${UID}\
	 --env GID=${GID}\
	 -w /app\
	 ${GITLAB_REGISTRY}/${GITLAB_PROJECT_BUILD}\

lint:
	golangci-lint run

doc:
	aglio --theme-variables slate -i doc/InventoryAPI.apib -o doc/InventoryAPI.html
	sed -i 's/VERSION_PLACEHOLDER/${VERSION}/g' doc/InventoryAPI.html

build:
	@go build ${LDFLAGS} -o inventory app/server/main.go
	@go build ${LDFLAGS} -o inventory-cli app/cli/main.go

test: lint
	go test ./... -count=1 -v

coverage: lint
	go test -coverpkg=./... -count=1 -v -coverprofile=/tmp/coverage.out ./...
	go tool cover -func=/tmp/coverage.out

package: clean build package-server package-cli

package-server: ${APP_DEB_PACKAGE_SERVER}
package-cli: ${APP_DEB_PACKAGE_CLI}

run-server:
	@echo './inventory runs in version ${VERSION}'
	@./inventory -migrations app/server/migrations -database app/server/inventory.db

clean:
	@rm -f inventory*.db
	@rm -f inventory*.deb
	@rm -f inventory
	@rm -f inventory-cli
	@rm -rf out/

install: package
	@sudo dpkg -i inventory*.deb

docker-image:
	@docker build -t ${GITLAB_REGISTRY}/${GITLAB_PROJECT_BUILD} -f docker/build.Dockerfile .

docker-gitlab-registry-push: docker-image
	docker login ${GITLAB_REGISTRY}
	docker push ${GITLAB_REGISTRY}/${GITLAB_PROJECT_BUILD}

docker-build:
	@docker run ${DOCKER_PARAM} /bin/bash -c "make build && chown ${UID}:${GID} inventory"

docker-test:
	@docker run ${DOCKER_PARAM} make test

docker-coverage:
	@docker run ${DOCKER_PARAM} make coverage

docker-doc:
	@docker run ${DOCKER_PARAM} make doc

docker-shell:
	@docker run ${DOCKER_PARAM} bash

docker-package:
	@docker run ${DOCKER_PARAM} /bin/bash -c "make package && chown ${UID}:${GID} inventory inventory*.deb && rm -rf out/"

docker-image-prod:
	@docker build -t ${GITLAB_REGISTRY}/${GITLAB_PROJECT_PROD_AMD64} -f docker/prod.amd64.Dockerfile .

docker-run-prod:
	@docker run --rm -d -p 2342:2342 -v inventory-db:/var/opt/inventory ${GITLAB_REGISTRY}/${GITLAB_PROJECT_PROD_AMD64}

.PHONY: lint\
	doc\
	build\
	test\
	coverage\
	package\
	run-server\
	clean\
	install\
	docker-image\
	docker-image-run\
	docker-doc\
	docker-shell\
	docker-gitlab-registry-push\
	docker-build\
	docker-test\
	docker-coverage\
	docker-package
