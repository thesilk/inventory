package config

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	yaml "gopkg.in/yaml.v2"
)

const (
	configContent = `
server_port: 2342
server_host: localhost
api_key: ""
`
)

func TestConfig(t *testing.T) {
	t.Run("read file", testReadFile)
	t.Run("read file with error", testReadFileWithError)
	t.Run("write config", testWriteFile)
	t.Run("write config with error", testWriteFileWithError)
	t.Run("need update if Cfg is nil", testUpdateConfigIfNil)
	t.Run("need update if Cfg is not nil", testUpdateConfigIfNotNil)
	t.Run("init config without config file", testInitConfigWOConfigFile)
	t.Run("init config with config file", testInitConfigWithConfigFile)
	t.Run("getter methods", testGetterMethods)
	t.Run("setter methods", testSetterMethods)
}

func writeTmpConfig() string {
	c := []byte(configContent)
	tempfile, err := ioutil.TempFile("", "config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	if _, err := tempfile.Write(c); err != nil {
		log.Fatal(err)
	}

	if err := tempfile.Close(); err != nil {
		log.Fatal(err)
	}

	return tempfile.Name()

}

func testReadFile(t *testing.T) {
	path := writeTmpConfig()
	defer os.Remove(path)

	expectedConfig := &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	err := readConfig(path)

	assert.Nil(t, err)
	assert.Equal(t, expectedConfig, Cfg)

	//cleanup
	Cfg = nil
}

func testReadFileWithError(t *testing.T) {
	err := readConfig("/tmp/config_not_exists.yaml")

	assert.Equal(t, "couldn't open config.yaml: open /tmp/config_not_exists.yaml: no such file or directory", err.Error())
	assert.Nil(t, Cfg)
}

func testWriteFile(t *testing.T) {
	Cfg = &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	path := "/tmp/config.yaml"

	err := WriteConfig(path)
	assert.Nil(t, err)
	defer os.Remove(path)

	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	var actualConfig *config

	err = yaml.Unmarshal(content, &actualConfig)

	assert.Nil(t, err)
	assert.Equal(t, Cfg, actualConfig)

	//cleanup
	Cfg = nil
}

func testWriteFileWithError(t *testing.T) {
	path := "/tmp/folder_avdkfdkfs/config.yaml"

	err := WriteConfig(path)
	assert.Equal(t, "couldn't write config file: open /tmp/folder_avdkfdkfs/config.yaml: no such file or directory", err.Error())
	defer os.Remove(path)
}

func testUpdateConfigIfNil(t *testing.T) {
	Cfg = nil
	expectedConfig := &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	needUpdate := UpdateConfig()

	assert.True(t, needUpdate)
	assert.Equal(t, expectedConfig, Cfg)

	Cfg = nil
}

func testUpdateConfigIfNotNil(t *testing.T) {
	Cfg = &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	needUpdate := UpdateConfig()

	assert.False(t, needUpdate)

	Cfg = nil
}

func testInitConfigWOConfigFile(t *testing.T) {
	expectedConfig := &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	const path = "/tmp/config2.yaml"
	err := InitConfig(path)
	os.Remove(path)

	assert.Nil(t, err)
	assert.Equal(t, expectedConfig, Cfg)
}

func testInitConfigWithConfigFile(t *testing.T) {
	expectedConfig := &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	path := writeTmpConfig()

	err := InitConfig(path)
	os.Remove(path)

	assert.Nil(t, err)
	assert.Equal(t, expectedConfig, Cfg)
}

func testGetterMethods(t *testing.T) {
	Cfg = &config{
		ServerHost: "localhost",
		ServerPort: 2342,
	}

	assert.Equal(t, "localhost", Cfg.GetServerHost())
	assert.Equal(t, 2342, Cfg.GetServerPort())

	Cfg = nil
}

func testSetterMethods(t *testing.T) {
	Cfg = &config{}

	Cfg.SetServerHost("localhost2")
	Cfg.SetServerPort(1234)

	expectedConfig := &config{
		ServerHost: "localhost2",
		ServerPort: 1234,
	}

	assert.Equal(t, expectedConfig, Cfg)
}
