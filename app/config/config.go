package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"

	yaml "gopkg.in/yaml.v2"
)

type config struct {
	ServerPort int    `yaml:"server_port"`
	ServerHost string `yaml:"server_host"`
}

var (
	// Cfg global variable to access config file
	Cfg   *config
	mutex sync.Mutex
)

// UpdateConfig updates config file
func UpdateConfig() bool {
	var needUpdate bool

	if Cfg == nil {
		Cfg = &config{
			ServerHost: "localhost",
			ServerPort: 2342,
		}
		needUpdate = true
	}

	return needUpdate
}

// InitConfig initializes config and create nested directories if needed
func InitConfig(configPath string) error {
	if _, err := os.Stat(configPath); !os.IsNotExist(err) {
		if err := readConfig(configPath); err != nil {
			return err
		}
	} else {
		if err := os.MkdirAll(filepath.Dir(configPath), os.ModePerm); err != nil {
			return err
		}
	}
	UpdateConfig()
	return WriteConfig(configPath)
}

func readConfig(configPath string) error {
	f, err := ioutil.ReadFile(configPath)
	if err != nil {
		return fmt.Errorf("couldn't open config.yaml: %v", err)
	}

	err = yaml.Unmarshal(f, &Cfg)
	if err != nil {
		return fmt.Errorf("couldn't read config.yaml: %v", err)
	}
	return nil
}

// WriteConfig overrides config file
func WriteConfig(configPath string) error {
	cfg, err := yaml.Marshal(&Cfg)
	if err != nil {
		return fmt.Errorf("couldn't marshal config: %v", err)
	}

	mutex.Lock()

	if err := ioutil.WriteFile(configPath, cfg, 0644); err != nil {
		mutex.Unlock()
		return fmt.Errorf("couldn't write config file: %v", err)
	}

	mutex.Unlock()

	return nil
}

// GetServerPort returns server port
func (c *config) GetServerPort() int {
	return c.ServerPort
}

// SetServerPort set new server port -> UpdateConfig is needed after setting new values
func (c *config) SetServerPort(port int) {
	c.ServerPort = port
}

// GetServerHost returns server host address
func (c *config) GetServerHost() string {
	return c.ServerHost
}

// SetServerHost sets new host to config -> UpdateConfig is needed after setting new values
func (c *config) SetServerHost(host string) {
	c.ServerHost = host
}
