package usecase

import (
	"fmt"

	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
)

// DeviceUsecase handles repository
type DeviceUsecase struct {
	repository interfaces.DevicePort
}

// NewDeviceUsecase creates device usecase struct
func NewDeviceUsecase(repository interfaces.DevicePort) DeviceUsecase {
	return DeviceUsecase{repository: repository}
}

// CreateDevice calls db method to create a new device item
func (u DeviceUsecase) CreateDevice(device *models.Device) (uint32, error) {
	return u.repository.CreateDevice(device)
}

// GetDevices calls db method to get all device item from db
func (u DeviceUsecase) GetDevices() ([]*models.Device, error) {
	return u.repository.GetDevices()
}

// GetDevice calls db method to get device item for given id from db
func (u DeviceUsecase) GetDevice(id uint64) (*models.Device, error) {
	return u.repository.GetDevice(id)
}

// UpdateDevice calls db method to update device item for given id from db
func (u DeviceUsecase) UpdateDevice(id uint64, device *models.Device) error {
	return u.repository.UpdateDevice(id, device)
}

// DeleteDevice calls db method to delete inventory item for given id from db
func (u DeviceUsecase) DeleteDevice(id uint64) error {
	return u.repository.DeleteDevice(id)
}

// UpdateLongTermUsage provides usecase to update longTermUsage for given device id
func (u DeviceUsecase) UpdateLongTermUsage(id uint64, longTermUsage bool) error {
	device, err := u.GetDevice(id)
	if err != nil {
		return fmt.Errorf("Error in update longTermUsage: %v", err)
	}

	device.LongTermUsage = longTermUsage

	return u.repository.UpdateDevice(id, device)
}
