package usecase

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
)

var expectedUser models.User = models.User{
	ID:   1,
	Name: "Test User",
	Role: "dev",
}

func TestUserUsecase(t *testing.T) {
	t.Run("create user", testCreateUser)
	t.Run("create user returns error", testCreateUserError)
	t.Run("get users", testGetUsers)
	t.Run("get users returns error", testGetUsersError)
	t.Run("get user", testGetUser)
	t.Run("get user returns error", testGetUserError)
	t.Run("update user", testUpdateUser)
	t.Run("update user returns error", testUpdateUserError)
	t.Run("delete user", testDeleteUser)
	t.Run("delete user returns error", testDeleteUserError)
}

func testCreateUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	var user *models.User
	err := u.CreateUser(user)
	assert.Nil(t, err)
}

func testCreateUserError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	var user *models.User
	err := u.CreateUser(user)
	assert.Equal(t, fmt.Errorf("couldn't create user"), err)
}

func testGetUsers(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	inventory, err := u.GetUsers()
	assert.Nil(t, err)
	assert.Equal(t, []*models.User{&expectedUser}, inventory)
}

func testGetUsersError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	inventory, err := u.GetUsers()
	assert.Equal(t, []*models.User{}, inventory)
	assert.Equal(t, fmt.Errorf("couldn't get users"), err)
}

func testGetUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	inventory, err := u.GetUser(1)
	assert.Nil(t, err)
	assert.Equal(t, &expectedUser, inventory)
}

func testGetUserError(t *testing.T) {
	var user *models.User
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	inventory, err := u.GetUser(1)
	assert.Equal(t, user, inventory)
	assert.Equal(t, fmt.Errorf("couldn't get user"), err)
}

func testUpdateUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	var user *models.User
	err := u.UpdateUser(1, user)
	assert.Nil(t, err)
}

func testUpdateUserError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	var user *models.User
	err := u.UpdateUser(1, user)
	assert.Equal(t, fmt.Errorf("couldn't update user"), err)
}

func testDeleteUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.DeleteUser(1)
	assert.Nil(t, err)
}

func testDeleteUserError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	err := u.DeleteUser(1)
	assert.Equal(t, fmt.Errorf("couldn't delete user"), err)
}
