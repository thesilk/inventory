package usecase

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	commonModel "gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
)

var expectedInventory commonModel.Inventory = commonModel.Inventory{
	ID:         1,
	Name:       "test machine",
	HostName:   "host name",
	IP:         "0.0.0.0",
	Port:       1234,
	Version:    "v1",
	UserName:   "peter",
	UserRole:   "developer",
	UpdateTime: 0,
}

func TestInventoryUsecase(t *testing.T) {
	t.Run("create inventoryItem", testCreateInventoryItem)
	t.Run("create inventoryItem returns error", testCreateInventoryItemError)
	t.Run("get inventory", testGetInventory)
	t.Run("get inventory returns error", testGetInventoryError)
	t.Run("get inventoryItem", testGetInventoryItem)
	t.Run("get inventoryItem returns error", testGetInventoryItemError)
	t.Run("update inventoryItem", testUpdateInventoryItem)
	t.Run("update inventoryItem returns error", testUpdateInventoryItemError)
	t.Run("delete inventoryItem", testDeleteInventoryItem)
	t.Run("delete inventoryItem returns error", testDeleteInventoryItemError)
	t.Run("reset inventory user", testResetInventoryUser)
	t.Run("reset inventory user returns error", testResetInventoryUserError)
	t.Run("reset inventory user (private method)", testResetAllInventoryItems)
	t.Run("reset inventory user returns error (private method)", testResetAllInventoryItemsError)
}

func testCreateInventoryItem(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	var state models.State
	err := u.CreateInventoryItem(&state)
	assert.Nil(t, err)
}

func testCreateInventoryItemError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	var state models.State
	err := u.CreateInventoryItem(&state)
	assert.Equal(t, fmt.Errorf("couldn't create state"), err)
}

func testGetInventory(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	inventory, err := u.GetInventory()
	assert.Nil(t, err)
	assert.Equal(t, []*commonModel.Inventory{&expectedInventory}, inventory)
}

func testGetInventoryError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	inventory, err := u.GetInventory()
	assert.Equal(t, []*commonModel.Inventory{}, inventory)
	assert.Equal(t, fmt.Errorf("couldn't get inventory"), err)
}

func testGetInventoryItem(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	inventory, err := u.GetInventoryItem(1)
	assert.Nil(t, err)
	assert.Equal(t, &expectedInventory, inventory)
}

func testGetInventoryItemError(t *testing.T) {
	var inventoryItem *commonModel.Inventory
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	inventory, err := u.GetInventoryItem(1)
	assert.Equal(t, inventoryItem, inventory)
	assert.Equal(t, fmt.Errorf("couldn't get inventory item"), err)
}

func testUpdateInventoryItem(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	err := u.UpdateInventoryItem(uint32(1), uint32(1))
	assert.Nil(t, err)
}

func testUpdateInventoryItemError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	err := u.UpdateInventoryItem(uint32(1), uint32(1))
	assert.Equal(t, fmt.Errorf("couldn't update state"), err)
}

func testDeleteInventoryItem(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	err := u.DeleteInventoryItem(1)
	assert.Nil(t, err)
}

func testDeleteInventoryItemError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	err := u.DeleteInventoryItem(1)
	assert.Equal(t, fmt.Errorf("couldn't delete state"), err)
}

func testResetInventoryUser(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	err := u.DeleteInventoryItem(1)
	assert.Nil(t, err)
}

func testResetInventoryUserError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	err := u.ResetInventoryUser()
	assert.Equal(t, fmt.Errorf("Couldn't get inventory items: couldn't get inventory"), err)
}

func testResetAllInventoryItems(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	err := u.resetAllInventoryItems([]*commonModel.Inventory{&expectedInventory})
	assert.Nil(t, err)
}

func testResetAllInventoryItemsError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	err := u.resetAllInventoryItems([]*commonModel.Inventory{&expectedInventory})
	assert.Equal(t, fmt.Errorf("Couldn't reset user for test machine: couldn't update state"), err)
}
