package usecase

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
)

var expectedDevice models.Device = models.Device{
	ID:       1,
	Name:     "Test Device",
	HostName: "Local Horst",
	IP:       "0.0.0.0",
	Port:     1337,
	Version:  "v2.1.6",
}

func TestDeviceUsecase(t *testing.T) {
	t.Run("create device", testCreateDevice)
	t.Run("create device returns error", testCreateDeviceError)
	t.Run("get devices", testGetDevices)
	t.Run("get devices returns error", testGetDevicesError)
	t.Run("get device", testGetDevice)
	t.Run("get device returns error", testGetDeviceError)
	t.Run("update device", testUpdateDevice)
	t.Run("update device returns error", testUpdateDeviceError)
	t.Run("delete device", testDeleteDevice)
	t.Run("delete device returns error", testDeleteDeviceError)
}

func testCreateDevice(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := NewDeviceUsecase(fakeAPI)

	var device *models.Device
	id, err := u.CreateDevice(device)
	assert.Nil(t, err)
	assert.Equal(t, uint32(1), id)
}

func testCreateDeviceError(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := NewDeviceUsecase(fakeAPI)

	var device *models.Device
	id, err := u.CreateDevice(device)
	assert.Equal(t, fmt.Errorf("couldn't create device"), err)
	assert.Equal(t, uint32(0), id)
}

func testGetDevices(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := NewDeviceUsecase(fakeAPI)

	inventory, err := u.GetDevices()
	assert.Nil(t, err)
	assert.Equal(t, []*models.Device{&expectedDevice}, inventory)
}

func testGetDevicesError(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := NewDeviceUsecase(fakeAPI)

	inventory, err := u.GetDevices()
	assert.Equal(t, []*models.Device{}, inventory)
	assert.Equal(t, fmt.Errorf("couldn't get devices"), err)
}

func testGetDevice(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := NewDeviceUsecase(fakeAPI)

	inventory, err := u.GetDevice(1)
	assert.Nil(t, err)
	assert.Equal(t, &expectedDevice, inventory)
}

func testGetDeviceError(t *testing.T) {
	var device *models.Device
	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := NewDeviceUsecase(fakeAPI)

	inventory, err := u.GetDevice(1)
	assert.Equal(t, device, inventory)
	assert.Equal(t, fmt.Errorf("couldn't get device"), err)
}

func testUpdateDevice(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := NewDeviceUsecase(fakeAPI)

	var device *models.Device
	err := u.UpdateDevice(1, device)
	assert.Nil(t, err)
}

func testUpdateDeviceError(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := NewDeviceUsecase(fakeAPI)

	var device *models.Device
	err := u.UpdateDevice(1, device)
	assert.Equal(t, fmt.Errorf("couldn't update device"), err)
}

func testDeleteDevice(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := NewDeviceUsecase(fakeAPI)

	err := u.DeleteDevice(1)
	assert.Nil(t, err)
}

func testDeleteDeviceError(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := NewDeviceUsecase(fakeAPI)

	err := u.DeleteDevice(1)
	assert.Equal(t, fmt.Errorf("couldn't delete device"), err)
}
