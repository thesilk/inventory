package usecase

import (
	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
)

// UserUsecase handles repository
type UserUsecase struct {
	repository interfaces.UserPort
}

// NewUserUsecase creates user usecase struct
func NewUserUsecase(repository interfaces.UserPort) UserUsecase {
	return UserUsecase{repository: repository}
}

// CreateUser calls db method to create a new user item
func (u UserUsecase) CreateUser(user *models.User) error {
	return u.repository.CreateUser(user)
}

// GetUsers calls db method to get all user item from db
func (u UserUsecase) GetUsers() ([]*models.User, error) {
	return u.repository.GetUsers()
}

// GetUser calls db method to get user item for given id from db
func (u UserUsecase) GetUser(id uint64) (*models.User, error) {
	return u.repository.GetUser(id)
}

// UpdateUser calls db method to update user for given id from db
func (u UserUsecase) UpdateUser(id uint64, user *models.User) error {
	return u.repository.UpdateUser(id, user)
}

// DeleteUser calls db method to delete user for given id from db
func (u UserUsecase) DeleteUser(id uint64) error {
	return u.repository.DeleteUser(id)
}
