package usecase

import (
	"fmt"
	"time"

	"gitlab.com/thesilk/inventory/app/models"
	serverModels "gitlab.com/thesilk/inventory/app/server/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
)

// InventoryUsecase handles repository
type InventoryUsecase struct {
	repository interfaces.InventoryPort
}

// NewInventoryUsecase creates device usecase struct
func NewInventoryUsecase(repository interfaces.InventoryPort) InventoryUsecase {
	return InventoryUsecase{repository: repository}
}

// GetInventory calls db method to get all device item from db
func (u InventoryUsecase) GetInventory() ([]*models.Inventory, error) {
	return u.repository.GetInventory()
}

// GetInventoryItem calls db method to get device item for given id from db
func (u InventoryUsecase) GetInventoryItem(id uint32) (*models.Inventory, error) {
	return u.repository.GetInventoryItem(id)
}

// UpdateInventoryItem calls db method to get device item for given id from db
func (u InventoryUsecase) UpdateInventoryItem(id, userID uint32) error {
	updateTime := uint32(time.Now().Unix())
	return u.repository.UpdateInventoryItem(id, userID, updateTime)
}

// DeleteInventoryItem deletes state for given id from db
func (u InventoryUsecase) DeleteInventoryItem(id uint32) error {
	return u.repository.DeleteInventoryItem(id)
}

// CreateInventoryItem creates given state
func (u InventoryUsecase) CreateInventoryItem(state *serverModels.State) error {
	state.UpdateTime = uint32(time.Now().Unix())
	return u.repository.CreateInventoryItem(state)
}

// ResetInventoryUser set all users of inventory items to None
func (u InventoryUsecase) ResetInventoryUser() error {
	inventory, err := u.repository.GetInventory()
	if err != nil {
		return fmt.Errorf("Couldn't get inventory items: %v", err)
	}

	return u.resetAllInventoryItems(inventory)
}

func (u InventoryUsecase) resetAllInventoryItems(inventory []*models.Inventory) error {
	updateTime := uint32(time.Now().Unix())

	for _, inventoryItem := range inventory {
		if inventoryItem.UserName != "none" && !inventoryItem.LongTermUsage {
			if err := u.repository.UpdateInventoryItem(inventoryItem.ID, 1, updateTime); err != nil {
				return fmt.Errorf("Couldn't reset user for %s: %v", inventoryItem.Name, err)
			}
		}
	}
	return nil
}
