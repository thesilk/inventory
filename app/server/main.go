package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"

	"github.com/robfig/cron/v3"
	"gitlab.com/thesilk/inventory/app/server/controller"

	"gitlab.com/thesilk/inventory/app/server/repository/sqlite3"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

var (
	version       string
	migrationPath string
	databasePath  string
)

func initCLI() {
	cwd, err := os.Getwd()
	if err != nil {
		log.Printf("couldn't get current working directory: %v", err)
	}

	helpFlag := flag.Bool("help", false, "help")
	versionFlag := flag.Bool("version", false, "version")
	flag.StringVar(&migrationPath, "migrations", fmt.Sprintf("%s/migrations", cwd), "path to inventory database migration directory")
	flag.StringVar(&databasePath, "database", fmt.Sprintf("%s/inventory.db", cwd), "path to inventory database")
	flag.Parse()

	if *helpFlag {
		flag.Usage()
		os.Exit(0)
	}

	if *versionFlag {
		fmt.Println(version)
		os.Exit(0)
	}
}

func registerResetInventoryUserCronJob(inventoryUsecase usecase.InventoryUsecase) {
	c := cron.New()
	_, err := c.AddFunc("0 0 * * *", func() {
		if err := inventoryUsecase.ResetInventoryUser(); err != nil {
			log.Printf("cron job error: %v", err)
		}
	})

	if err != nil {
		panic("Couldn't add reset inventory user job")
	}
	c.Start()
	log.Println("register cronjob to reset inventory user relationship was successfully")
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	initCLI()

	adapter := controller.NewAdapter()
	repository := sqlite3.NewRepository(migrationPath, databasePath)

	deviceUsecase := usecase.NewDeviceUsecase(repository)
	userUsecase := usecase.NewUserUsecase(repository)
	inventoryUsecase := usecase.NewInventoryUsecase(repository)

	registerResetInventoryUserCronJob(inventoryUsecase)

	createDeviceHandler := adapter.MakeCreateDevice(deviceUsecase, inventoryUsecase)
	getDevicesHandler := adapter.MakeGetDevices(deviceUsecase)
	getDeviceHandler := adapter.MakeGetDevice(deviceUsecase)
	updateDeviceHandler := adapter.MakeUpdateDevice(deviceUsecase)
	deleteDeviceHandler := adapter.MakeDeleteDevice(deviceUsecase, inventoryUsecase)

	createUserHandler := adapter.MakeCreateUser(userUsecase)
	getUsersHandler := adapter.MakeGetUsers(userUsecase)
	getUserHandler := adapter.MakeGetUser(userUsecase)
	updateUserHandler := adapter.MakeUpdateUser(userUsecase)
	deleteUserHandler := adapter.MakeDeleteUser(userUsecase)

	getInventoryHandler := adapter.MakeGetInventory(inventoryUsecase)
	getInventoryItemHandler := adapter.MakeGetInventoryItem(inventoryUsecase)
	updateInventoryItemHandler := adapter.MakeUpdateInventoryItem(inventoryUsecase)

	adapter.HandleFunc("/api/v0/devices", getDevicesHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/devices", createDeviceHandler).Methods("POST")
	adapter.HandleFunc("/api/v0/device/{id}", getDeviceHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/device/{id}", updateDeviceHandler).Methods("PUT")
	adapter.HandleFunc("/api/v0/device/{id}", deleteDeviceHandler).Methods("DELETE")

	adapter.HandleFunc("/api/v0/users", getUsersHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/users", createUserHandler).Methods("POST")
	adapter.HandleFunc("/api/v0/user/{id}", getUserHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/user/{id}", updateUserHandler).Methods("PUT")
	adapter.HandleFunc("/api/v0/user/{id}", deleteUserHandler).Methods("DELETE")

	adapter.HandleFunc("/api/v0/inventory", getInventoryHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/inventory/{id}", getInventoryItemHandler).Methods("GET")
	adapter.HandleFunc("/api/v0/inventory/{id}", updateInventoryItemHandler).Methods("PUT")

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	go func() {
		adapter.ListenAndServe()
	}()
	<-stop

	fmt.Println()
	log.Println("Shutting down the server ...")
	adapter.Shutdown()
	log.Println("Server gracefully stopped")
}
