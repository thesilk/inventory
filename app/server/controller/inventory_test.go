package controller

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	commonModel "gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

var expectedInventoryItem commonModel.Inventory = commonModel.Inventory{
	ID:         1,
	Name:       "test machine",
	HostName:   "host name",
	IP:         "0.0.0.0",
	Port:       1234,
	Version:    "v1",
	UserName:   "peter",
	UserRole:   "developer",
	UpdateTime: 0,
}

func TestInventoryController(t *testing.T) {
	t.Run("get inventory", testGetInventory)
	t.Run("get inventory returns error", testGetInventoryError)
	t.Run("get inventory item", testGetInventoryItem)
	t.Run("get inventory item returns error", testGetInventoryItemError)
	t.Run("get inventory item returns error wrong id", testGetInventoryItemIDError)
	t.Run("update inventory item", testUpdateInventoryItem)
	t.Run("update inventory item returns error (bad request)", testUpdateInventoryItemParameterError)
	t.Run("update inventory item returns error", testUpdateInventoryItemError)
	t.Run("update inventory item returns error wrong id", testUpdateInventoryItemIDError)
}

func testGetInventory(t *testing.T) {
	inventory := []commonModel.Inventory{}

	fakeAPI := interfaces.FakeInventory{}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetInventory(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &inventory)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, []commonModel.Inventory{expectedInventoryItem}, inventory)
}

func testGetInventoryError(t *testing.T) {
	inventory := []commonModel.Inventory{}

	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetInventory(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &inventory)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, []commonModel.Inventory{}, inventory)
}

func testGetInventoryItem(t *testing.T) {
	inventoryItem := commonModel.Inventory{}

	fakeAPI := interfaces.FakeInventory{}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetInventoryItem(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &inventoryItem)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, expectedInventoryItem, inventoryItem)
}

func testGetInventoryItemError(t *testing.T) {
	inventoryItem := commonModel.Inventory{}

	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetInventoryItem(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &inventoryItem)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, commonModel.Inventory{}, inventoryItem)
}

func testGetInventoryItemIDError(t *testing.T) {
	inventoryItem := commonModel.Inventory{}

	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/a", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "a"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetInventoryItem(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &inventoryItem)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, commonModel.Inventory{}, inventoryItem)
}

func testUpdateInventoryItem(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/1?userId=1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateInventoryItem(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusNoContent, response.Code)
}

func testUpdateInventoryItemError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/1?userId=1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateInventoryItem(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
}

func testUpdateInventoryItemParameterError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/1?userId=a", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateInventoryItem(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}

func testUpdateInventoryItemIDError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := usecase.NewInventoryUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/inventory/a", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "a"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateInventoryItem(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusBadRequest, response.Code)
}
