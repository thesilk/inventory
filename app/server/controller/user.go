package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

// MakeCreateUser provides POST handler to create a new user
func (a Adapter) MakeCreateUser(usecase usecase.UserUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user *models.User

		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't read body from request: %v", err))
			return
		}

		if err := json.Unmarshal(body, &user); err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't unmarshal body: %v", err))
			return
		}

		err = usecase.CreateUser(user)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't create user item: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusCreated, nil)
	}
}

// MakeGetUsers provides GET handler to get all users
func (a Adapter) MakeGetUsers(usecase usecase.UserUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		users, err := usecase.GetUsers()
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get all users: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, users)
	}
}

// MakeGetUser provides GET handler to get an user with a given id
func (a Adapter) MakeGetUser(usecase usecase.UserUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		user, err := usecase.GetUser(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get user item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, user)
	}
}

// MakeDeleteUser provides DELETE handler to delete an user with a given id
func (a Adapter) MakeDeleteUser(usecase usecase.UserUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		err = usecase.DeleteUser(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't delete user item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusNoContent, nil)
	}
}

// MakeUpdateUser provides PUT handler to update a user
func (a Adapter) MakeUpdateUser(usecase usecase.UserUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user *models.User

		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't read body from request: %v", err))
			return
		}

		if err := json.Unmarshal(body, &user); err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't unmarshal body: %v", err))
			return
		}

		err = usecase.UpdateUser(id, user)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't update user item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, nil)
	}
}
