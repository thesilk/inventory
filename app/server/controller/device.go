package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/thesilk/inventory/app/models"
	serverModels "gitlab.com/thesilk/inventory/app/server/models"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

// MakeCreateDevice provides POST handler to create a new device
func (a Adapter) MakeCreateDevice(uDevice usecase.DeviceUsecase, uInventory usecase.InventoryUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var device *models.Device
		var state serverModels.State

		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't read body from request: %v", err))
			return
		}

		if err := json.Unmarshal(body, &device); err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't unmarshal body: %v", err))
			return
		}

		id, err := uDevice.CreateDevice(device)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't create device item: %v", err))
			return
		}

		state.InventoryID = uint32(id)
		state.UserID = uint32(1)
		err = uInventory.CreateInventoryItem(&state)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't create device item: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusCreated, nil)
	}
}

// MakeGetDevices provides GET handler to get all devices
func (a Adapter) MakeGetDevices(usecase usecase.DeviceUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		devices, err := usecase.GetDevices()
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get all devices: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, devices)
	}
}

// MakeGetDevice provides GET handler to get an device with a given id
func (a Adapter) MakeGetDevice(usecase usecase.DeviceUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		device, err := usecase.GetDevice(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get device item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, device)
	}
}

// MakeDeleteDevice provides DELETE handler to delete an device with a given id
func (a Adapter) MakeDeleteDevice(uDevice usecase.DeviceUsecase, uInventory usecase.InventoryUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		err = uDevice.DeleteDevice(id)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't delete device item with id %d: %v", id, err))
			return
		}

		err = uInventory.DeleteInventoryItem(uint32(id))
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't delete inventory item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusNoContent, nil)
	}
}

// MakeUpdateDevice provides PUT handler to update a device
func (a Adapter) MakeUpdateDevice(usecase usecase.DeviceUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var (
			device        *models.Device
			longTermUsage bool = false
			longTermPatch bool = false
			err           error
		)

		if val := r.URL.Query().Get("longTerm"); len(val) > 0 {
			if longTermUsage, err = strconv.ParseBool(val); err != nil {
				a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't cast longTerm to bool: %v", err))
				return
			}
			longTermPatch = true
		}

		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		if longTermPatch {
			err = usecase.UpdateLongTermUsage(id, longTermUsage)
			if err != nil {
				a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't update long term usage for device item with id %d: %v", id, err))
				return
			}
		} else {
			body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
			if err != nil {
				a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't read body from request: %v", err))
				return
			}

			if err := json.Unmarshal(body, &device); err != nil {
				a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't unmarshal body: %v", err))
				return
			}

			err = usecase.UpdateDevice(id, device)
			if err != nil {
				a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't update device item with id %d: %v", id, err))
				return
			}
		}
		a.NewAPISuccess(w, http.StatusOK, nil)
	}
}
