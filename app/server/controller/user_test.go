package controller

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

var expectedUser models.User = models.User{
	ID:   1,
	Name: "Test User",
	Role: "dev",
}

func TestUserController(t *testing.T) {
	t.Run("get users", testGetUsers)
	t.Run("get users returns error", testGetUsersError)
	t.Run("get user", testGetUser)
	t.Run("get user returns error (usecase)", testGetUserUsecaseError)
	t.Run("get user returns error (url parameter)", testGetUserParameterError)
	t.Run("delete user", testDeleteUser)
	t.Run("delete user returns error (usecase)", testDeleteUserUsecaseError)
	t.Run("delete user returns error (url parameter)", testDeleteUserParameterError)
	t.Run("create user", testCreateUser)
	t.Run("create user returns error (usecase)", testCreateUserUsecaseError)
	t.Run("create user returns error (json)", testCreateUserInputError)
	t.Run("update user", testUpdateUser)
	t.Run("update user returns error (usecase)", testUpdateUserUsecaseError)
	t.Run("update user returns error (json)", testUpdateUserInputError)
	t.Run("update user returns error (url parameter)", testUpdateUserParameterError)
}

func testGetUsers(t *testing.T) {
	user := []models.User{}

	fakeAPI := interfaces.FakeUser{}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/users", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetUsers(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &user)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, []models.User{expectedUser}, user)
}

func testGetUsersError(t *testing.T) {
	user := []models.User{}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/users", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetUsers(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &user)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, []models.User{}, user)
}

func testGetUser(t *testing.T) {
	user := models.User{}

	fakeAPI := interfaces.FakeUser{}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/user/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &user)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, expectedUser, user)
}

func testGetUserUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't get user item with id 1: couldn't get user"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/user/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)

}

func testGetUserParameterError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't convert id parameter: strconv.ParseUint: parsing \"cb\": invalid syntax"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/user/cb", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "cb"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testDeleteUser(t *testing.T) {
	user := models.User{}
	fakeAPI := interfaces.FakeUser{}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("DELETE", "api/v0/user/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &user)

	assert.Equal(t, http.StatusNoContent, response.Code)
}

func testDeleteUserUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't delete user item with id 1: couldn't delete user"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("DELETE", "api/v0/user/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)

}

func testDeleteUserParameterError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't convert id parameter: strconv.ParseUint: parsing \"cb\": invalid syntax"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	r, _ := http.NewRequest("DELETE", "api/v0/user/cb", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "cb"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testCreateUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedUser)

	r, _ := http.NewRequest("POST", "api/v0/user", bytes.NewBuffer(reqBody))

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateUser(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusCreated, response.Code)
}

func testCreateUserUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't create user item: couldn't create user"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedUser)
	r, _ := http.NewRequest("POST", "api/v0/user", bytes.NewBuffer(reqBody))

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testCreateUserInputError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't unmarshal body: json: cannot unmarshal array into Go value of type models.User"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal([]models.User{})
	r, _ := http.NewRequest("POST", "api/v0/user", bytes.NewBuffer(reqBody))

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedUser)
	r, _ := http.NewRequest("PUT", "api/v0/user/1", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateUser(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusOK, response.Code)
}

func testUpdateUserUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't update user item with id 1: couldn't update user"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedUser)
	r, _ := http.NewRequest("PUT", "api/v0/user/1", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateUserInputError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't unmarshal body: json: cannot unmarshal array into Go value of type models.User"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal([]models.User{})
	r, _ := http.NewRequest("PUT", "api/v0/user/1", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateUserParameterError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't convert id parameter: strconv.ParseUint: parsing \"cb\": invalid syntax"}

	fakeAPI := interfaces.FakeUser{HasError: true}
	u := usecase.NewUserUsecase(fakeAPI)

	reqBody, _ := json.Marshal([]models.User{})
	r, _ := http.NewRequest("PUT", "api/v0/user/cb", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "cb"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateUser(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}
