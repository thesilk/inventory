package controller

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/repository/interfaces"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

var expectedDevice models.Device = models.Device{
	ID:       1,
	Name:     "Test Device",
	HostName: "Local Horst",
	IP:       "0.0.0.0",
	Port:     1337,
	Version:  "v2.1.6",
}

type errResponse struct {
	Error string `json:"error"`
}

func TestDeviceController(t *testing.T) {
	t.Run("get devices", testGetDevices)
	t.Run("get devices returns error", testGetDevicesError)
	t.Run("get device", testGetDevice)
	t.Run("get device returns error (usecase)", testGetDeviceUsecaseError)
	t.Run("get device returns error (url parameter)", testGetDeviceParameterError)
	t.Run("delete device", testDeleteDevice)
	t.Run("delete device returns error (usecase)", testDeleteDeviceUsecaseError)
	t.Run("delete device returns error (url parameter)", testDeleteDeviceParameterError)
	t.Run("create device", testCreateDevice)
	t.Run("create device returns error (usecase)", testCreateDeviceUsecaseError)
	t.Run("create device returns error (json)", testCreateDeviceInputError)
	t.Run("update device", testUpdateDevice)
	t.Run("update device returns error (usecase)", testUpdateDeviceUsecaseError)
	t.Run("update device returns error (json)", testUpdateDeviceInputError)
	t.Run("update device returns error (url parameter)", testUpdateDeviceParameterError)
	t.Run("update long term usage", testUpdateLongTermUsage)
	t.Run("update long term usage with non bool input", testUpdateLongTermUsageQueryError)
	t.Run("update long term usage with usecase error", testUpdateLongTermUsageUsecaseError)
}

func testGetDevices(t *testing.T) {
	device := []models.Device{}

	fakeAPI := interfaces.FakeDevice{}
	u := usecase.NewDeviceUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/devices", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetDevices(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &device)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, []models.Device{expectedDevice}, device)
}

func testGetDevicesError(t *testing.T) {
	device := []models.Device{}

	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/devices", nil)

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetDevices(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &device)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, []models.Device{}, device)
}

func testGetDevice(t *testing.T) {
	device := models.Device{}

	fakeAPI := interfaces.FakeDevice{}
	u := usecase.NewDeviceUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/device/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &device)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, expectedDevice, device)
}

func testGetDeviceUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't get device item with id 1: couldn't get device"}

	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/device/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)

}

func testGetDeviceParameterError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't convert id parameter: strconv.ParseUint: parsing \"cb\": invalid syntax"}

	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	r, _ := http.NewRequest("GET", "api/v0/device/cb", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "cb"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeGetDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testDeleteDevice(t *testing.T) {
	device := []models.Device{}
	fakeAPIDevice := interfaces.FakeDevice{}
	fakeAPIInventory := interfaces.FakeInventory{}
	uDevice := usecase.NewDeviceUsecase(fakeAPIDevice)
	uInventory := usecase.NewInventoryUsecase(fakeAPIInventory)

	r, _ := http.NewRequest("DELETE", "api/v0/device/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteDevice(uDevice, uInventory))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &device)

	assert.Equal(t, http.StatusNoContent, response.Code)
}

func testDeleteDeviceUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't delete device item with id 1: couldn't delete device"}

	fakeAPIDevice := interfaces.FakeDevice{HasError: true}
	fakeAPIInventory := interfaces.FakeInventory{}
	uDevice := usecase.NewDeviceUsecase(fakeAPIDevice)
	uInventory := usecase.NewInventoryUsecase(fakeAPIInventory)

	r, _ := http.NewRequest("DELETE", "api/v0/device/1", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteDevice(uDevice, uInventory))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)

}

func testDeleteDeviceParameterError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't convert id parameter: strconv.ParseUint: parsing \"cb\": invalid syntax"}

	fakeAPIDevice := interfaces.FakeDevice{}
	fakeAPIInventory := interfaces.FakeInventory{}
	uDevice := usecase.NewDeviceUsecase(fakeAPIDevice)
	uInventory := usecase.NewInventoryUsecase(fakeAPIInventory)

	r, _ := http.NewRequest("DELETE", "api/v0/device/cb", nil)
	r = mux.SetURLVars(r, map[string]string{"id": "cb"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeDeleteDevice(uDevice, uInventory))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testCreateDevice(t *testing.T) {
	fakeAPIDevice := interfaces.FakeDevice{}
	fakeAPIInventory := interfaces.FakeInventory{}
	uDevice := usecase.NewDeviceUsecase(fakeAPIDevice)
	uInventory := usecase.NewInventoryUsecase(fakeAPIInventory)

	reqBody, _ := json.Marshal(expectedDevice)

	r, _ := http.NewRequest("POST", "api/v0/device", bytes.NewBuffer(reqBody))

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateDevice(uDevice, uInventory))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusCreated, response.Code)
}

func testCreateDeviceUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't create device item: couldn't create device"}

	fakeAPIDevice := interfaces.FakeDevice{HasError: true}
	fakeAPIInventory := interfaces.FakeInventory{}
	uDevice := usecase.NewDeviceUsecase(fakeAPIDevice)
	uInventory := usecase.NewInventoryUsecase(fakeAPIInventory)

	reqBody, _ := json.Marshal(expectedDevice)
	r, _ := http.NewRequest("POST", "api/v0/device", bytes.NewBuffer(reqBody))

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateDevice(uDevice, uInventory))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testCreateDeviceInputError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't unmarshal body: json: cannot unmarshal array into Go value of type models.Device"}

	fakeAPIDevice := interfaces.FakeDevice{}
	fakeAPIInventory := interfaces.FakeInventory{}
	uDevice := usecase.NewDeviceUsecase(fakeAPIDevice)
	uInventory := usecase.NewInventoryUsecase(fakeAPIInventory)

	reqBody, _ := json.Marshal([]models.Device{})
	r, _ := http.NewRequest("POST", "api/v0/device", bytes.NewBuffer(reqBody))

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeCreateDevice(uDevice, uInventory))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateDevice(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedDevice)
	r, _ := http.NewRequest("PUT", "api/v0/device/1", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusOK, response.Code)
}

func testUpdateDeviceUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't update device item with id 1: couldn't update device"}

	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedDevice)
	r, _ := http.NewRequest("PUT", "api/v0/device/1", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateDeviceInputError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't unmarshal body: json: cannot unmarshal array into Go value of type models.Device"}

	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal([]models.Device{})
	r, _ := http.NewRequest("PUT", "api/v0/device/1", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateDeviceParameterError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't convert id parameter: strconv.ParseUint: parsing \"cb\": invalid syntax"}

	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal([]models.Device{})
	r, _ := http.NewRequest("PUT", "api/v0/device/cb", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "cb"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateLongTermUsage(t *testing.T) {
	fakeAPI := interfaces.FakeDevice{}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedDevice)
	r, _ := http.NewRequest("PUT", "api/v0/device/1?longTerm=true", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	assert.Equal(t, http.StatusOK, response.Code)
}

func testUpdateLongTermUsageQueryError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't cast longTerm to bool: strconv.ParseBool: parsing \"abc\": invalid syntax"}
	fakeAPI := interfaces.FakeDevice{}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedDevice)
	r, _ := http.NewRequest("PUT", "api/v0/device/1?longTerm=abc", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusBadRequest, response.Code)
	assert.Equal(t, errJSON, expectedError)
}

func testUpdateLongTermUsageUsecaseError(t *testing.T) {
	errJSON := errResponse{}
	expectedError := errResponse{Error: "couldn't update long term usage for device item with id 1: Error in update longTermUsage: couldn't get device"}
	fakeAPI := interfaces.FakeDevice{HasError: true}
	u := usecase.NewDeviceUsecase(fakeAPI)

	reqBody, _ := json.Marshal(expectedDevice)
	r, _ := http.NewRequest("PUT", "api/v0/device/1?longTerm=true", bytes.NewBuffer(reqBody))
	r = mux.SetURLVars(r, map[string]string{"id": "1"})

	response := httptest.NewRecorder()
	adapter := NewAdapter()
	handler := http.HandlerFunc(adapter.MakeUpdateDevice(u))
	handler.ServeHTTP(response, r)

	body, _ := ioutil.ReadAll(response.Body)
	_ = json.Unmarshal(body, &errJSON)

	assert.Equal(t, http.StatusInternalServerError, response.Code)
	assert.Equal(t, errJSON, expectedError)
}
