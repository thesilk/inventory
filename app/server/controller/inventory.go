package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"gitlab.com/thesilk/inventory/app/server/usecase"
)

type user struct {
	UserID uint32 `json:"userId"`
}

// MakeGetInventory provides GET handler to get all device-user-relationships
func (a Adapter) MakeGetInventory(usecase usecase.InventoryUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		inventory, err := usecase.GetInventory()
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get all device-user-relationships: %v", err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, inventory)
	}
}

// MakeGetInventoryItem provides GET handler to get the device-user-relationship for given device id
func (a Adapter) MakeGetInventoryItem(usecase usecase.InventoryUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		inventoryItem, err := usecase.GetInventoryItem(uint32(id))
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't get device-user-relationship item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusOK, inventoryItem)
	}
}

// MakeUpdateInventoryItem provides UPDATE handler to update inventor for given device id
func (a Adapter) MakeUpdateInventoryItem(usecase usecase.InventoryUsecase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user user

		if err := schema.NewDecoder().Decode(&user, r.URL.Query()); err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("missing url parameter 'userId': %v", err))
			return
		}
		id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)

		if err != nil {
			a.NewAPIError(w, http.StatusBadRequest, fmt.Sprintf("couldn't convert id parameter: %v", err))
			return
		}

		err = usecase.UpdateInventoryItem(uint32(id), user.UserID)
		if err != nil {
			a.NewAPIError(w, http.StatusInternalServerError, fmt.Sprintf("couldn't delete device item with id %d: %v", id, err))
			return
		}

		a.NewAPISuccess(w, http.StatusNoContent, nil)
	}
}
