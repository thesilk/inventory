package controller

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

//API struct
type API struct{}

//Adapter handles server and router
type Adapter struct {
	r      *mux.Router
	server *http.Server
}

//NewAdapter creates Adapter instance
func NewAdapter() Adapter {
	// serverCfg := fmt.Sprintf("%s:%d", config.Cfg.GetServerHost(), config.Cfg.GetServerPort())
	serverCfg := "0.0.0.0:2342"

	r := mux.NewRouter()
	adapter := Adapter{r, &http.Server{Addr: serverCfg, Handler: handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(r)}}
	return adapter
}

//ListenAndServe start server
func (a Adapter) ListenAndServe() {
	log.Printf("listening on %s", a.server.Addr)
	log.Fatal(a.server.ListenAndServe())
}

//Shutdown closes server gracefully
func (a Adapter) Shutdown() {
	_ = a.server.Shutdown(context.Background())
}

//HandleFunc creates handler route
func (a Adapter) HandleFunc(path string, f func(http.ResponseWriter, *http.Request)) *mux.Route {
	return a.r.NewRoute().Path(path).HandlerFunc(f)
}

//NewAPIError returns api error json
func (a Adapter) NewAPIError(w http.ResponseWriter, status int, message string) {
	respondWithJSON(w, status, map[string]string{"error": message})
}

//NewAPISuccess returns api success json
func (a Adapter) NewAPISuccess(w http.ResponseWriter, status int, payload interface{}) {
	respondWithJSON(w, status, payload)
}

func respondWithJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_, _ = w.Write(response)
}
