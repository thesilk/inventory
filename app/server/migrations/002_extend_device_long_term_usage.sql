-- +goose Up
CREATE TABLE IF NOT EXISTS device_copy (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        host_name TEXT,
        ip TEXT,
        port INTEGER,
        version TEXT,
        long_term_usage TEXT DEFAULT false);

INSERT INTO device_copy(id, name, host_name, ip, port, version) SELECT id, name, host_name, ip, port, version FROM device;

DROP TABLE device;
ALTER TABLE device_copy RENAME TO device;
