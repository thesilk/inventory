-- +goose Up
CREATE TABLE IF NOT EXISTS device (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT,
  host_name TEXT,
  ip TEXT,
  port INTEGER,
  version TEXT);

CREATE TABLE IF NOT EXISTS user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT,
  role TEXT);

CREATE TABLE IF NOT EXISTS state (
  inventory_id INTEGER,
  user_id INTEGER,
  update_time INTEGER);

INSERT INTO user (name, role) VALUES ("none", "none");
