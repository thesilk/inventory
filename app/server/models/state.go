package models

// State struct
type State struct {
	InventoryID uint32 `json:"inventory_id"`
	UserID      uint32 `json:"user_id"`
	UpdateTime  uint32 `json:"update_time"`
}
