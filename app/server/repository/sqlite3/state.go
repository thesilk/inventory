package sqlite3

import (
	"fmt"

	commonModel "gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/models"
)

// GetInventoryItem get state id db entry
func (m *Repository) GetInventoryItem(id uint32) (*commonModel.Inventory, error) {
	var inventoryItem commonModel.Inventory
	err := m.DB.QueryRow(`
			SELECT
				device.id,
				device.name,
				device.host_name,
				device.ip,
				device.port,
				device.version,
				device.long_term_usage,
				user.name,
				user.role,
				state.update_time
			FROM
				state
			INNER JOIN
				user
			ON
				user.id = state.user_id
			INNER JOIN
				device
			ON
			device.id = state.inventory_id
			WHERE
			inventory_id=$1`, id).Scan(
		&inventoryItem.ID,
		&inventoryItem.Name,
		&inventoryItem.HostName,
		&inventoryItem.IP,
		&inventoryItem.Port,
		&inventoryItem.Version,
		&inventoryItem.LongTermUsage,
		&inventoryItem.UserName,
		&inventoryItem.UserRole,
		&inventoryItem.UpdateTime,
	)
	if err != nil {
		return &inventoryItem, fmt.Errorf("Couldn't get inventory item: %v", err)
	}

	return &inventoryItem, nil
}

// GetInventory returns all users
func (m *Repository) GetInventory() ([]*commonModel.Inventory, error) {
	var inventory []*commonModel.Inventory

	rows, err := m.DB.Query(`
			SELECT
				device.id,
				device.name,
				device.host_name,
				device.ip,
				device.port,
				device.version,
				device.long_term_usage,
				user.name,
				user.role,
				state.update_time
			FROM
				state
			INNER JOIN
				user
			ON
				user.id = state.user_id
			INNER JOIN
				device
			ON
				device.id = state.inventory_id;`)
	if err != nil {
		return inventory, fmt.Errorf("Couldn't get inventory information: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var inventoryItem commonModel.Inventory
		err := rows.Scan(
			&inventoryItem.ID,
			&inventoryItem.Name,
			&inventoryItem.HostName,
			&inventoryItem.IP,
			&inventoryItem.Port,
			&inventoryItem.Version,
			&inventoryItem.LongTermUsage,
			&inventoryItem.UserName,
			&inventoryItem.UserRole,
			&inventoryItem.UpdateTime,
		)
		if err != nil {
			return []*commonModel.Inventory{}, fmt.Errorf("Couldn't scan state row: %v", err)
		}

		inventory = append(inventory, &inventoryItem)
	}

	return inventory, nil
}

// UpdateInventoryItem set user to inventory item
func (m *Repository) UpdateInventoryItem(inventoryID, userID, updateTime uint32) error {
	stmt, err := m.DB.Prepare(`
		UPDATE state SET
				user_id=?,
				update_time=? WHERE inventory_id=?`)

	if err != nil {
		return fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(userID, updateTime, inventoryID)
	if err != nil {
		return fmt.Errorf("Couldn't execute device insert statement: %v", err)
	}

	return nil
}

// CreateInventoryItem creates new user-device-relationship
func (m *Repository) CreateInventoryItem(state *models.State) error {
	stmt, err := m.DB.Prepare(`INSERT INTO state (inventory_id, user_id, update_time) VALUES (?,?,?)`)

	if err != nil {
		return fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&state.InventoryID,
		&state.UserID,
		&state.UpdateTime,
	)
	if err != nil {
		return fmt.Errorf("Couldn't execute state insert statement: %v", err)
	}

	return nil
}

// DeleteInventoryItem creates new user-device-relationship
func (m *Repository) DeleteInventoryItem(inventoryID uint32) error {
	stmt, err := m.DB.Prepare("DELETE FROM state where inventory_id=?")
	if err != nil {
		return fmt.Errorf("Couldn't pepare statement for deleting state with device id %d: %v", inventoryID, err)
	}

	_, err = stmt.Exec(inventoryID)
	if err != nil {
		return fmt.Errorf("Couldn't execute query for deleting state with device id %d: %v", inventoryID, err)
	}

	return nil
}
