package sqlite3

import (
	"fmt"

	"gitlab.com/thesilk/inventory/app/models"
)

// GetDevice returns given device
func (m *Repository) GetDevice(id uint64) (*models.Device, error) {
	var device models.Device
	err := m.DB.QueryRow("SELECT * FROM device where id=$1", id).Scan(
		&device.ID,
		&device.Name,
		&device.HostName,
		&device.IP,
		&device.Port,
		&device.Version,
		&device.LongTermUsage,
	)
	if err != nil {
		return &device, fmt.Errorf("Couldn't get device item with id %d: %v", id, err)
	}

	return &device, nil
}

// GetDevices returns all devices
func (m *Repository) GetDevices() ([]*models.Device, error) {
	var devices []*models.Device

	rows, err := m.DB.Query("SELECT * from device")
	if err != nil {
		return devices, fmt.Errorf("Couldn't get device information: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var device models.Device
		err := rows.Scan(
			&device.ID,
			&device.Name,
			&device.HostName,
			&device.IP,
			&device.Port,
			&device.Version,
			&device.LongTermUsage,
		)
		if err != nil {
			return []*models.Device{}, fmt.Errorf("Couldn't scan device row: %v", err)
		}

		devices = append(devices, &device)
	}

	return devices, nil
}

// CreateDevice returns given device
func (m *Repository) CreateDevice(device *models.Device) (uint32, error) {
	stmt, err := m.DB.Prepare(`INSERT INTO device (name, host_name, ip, port, version, long_term_usage) VALUES (?,?,?,?,?,?)`)

	if err != nil {
		return uint32(0), fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	res, err := stmt.Exec(
		&device.Name,
		&device.HostName,
		&device.IP,
		&device.Port,
		&device.Version,
		&device.LongTermUsage,
	)
	if err != nil {
		return uint32(0), fmt.Errorf("Couldn't execute device insert statement: %v", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return uint32(0), fmt.Errorf("Couldn't get last insert id: %v", err)
	}

	return uint32(id), err
}

//UpdateDevice updates given device
func (m *Repository) UpdateDevice(id uint64, device *models.Device) error {
	stmt, err := m.DB.Prepare(`
		UPDATE device SET
				name=?,
				host_name=?,
				ip=?,
				port=?,
				version=?,
				long_term_usage=? WHERE id=?`)

	if err != nil {
		return fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&device.Name,
		&device.HostName,
		&device.IP,
		&device.Port,
		&device.Version,
		&device.LongTermUsage,
		id,
	)
	if err != nil {
		return fmt.Errorf("Couldn't execute device insert statement: %v", err)
	}

	return nil
}

//DeleteDevice deletes given device
func (m *Repository) DeleteDevice(id uint64) error {
	stmt, err := m.DB.Prepare("DELETE FROM device where id=?")
	if err != nil {
		return fmt.Errorf("Couldn't pepare statement for deleting device with id %d: %v", id, err)
	}

	_, err = stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("Couldn't execute query for deleting device with id %d: %v", id, err)
	}

	return nil
}
