package sqlite3

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
)

var expectedDevices []*models.Device = []*models.Device{
	{
		ID:            1,
		Name:          "production",
		HostName:      "production.the.silk.host.com",
		IP:            "0.0.0.0",
		Port:          137,
		Version:       "v1.0.0",
		LongTermUsage: false,
	}, {

		ID:            2,
		Name:          "staging",
		HostName:      "staging.the.silk.host.com",
		IP:            "127.0.0.1",
		Port:          1337,
		Version:       "v1.0.1",
		LongTermUsage: true,
	},
}

func TestDeviceRepository(t *testing.T) {
	t.Run("get device", testGetDevice)
	t.Run("get device error for non existing device", testGetDeviceError)
	t.Run("get devices", testGetDevices)
	t.Run("delete device", testDeleteDevice)
	t.Run("create device", testCreateDevice)
	t.Run("update device", testUpdateDevice)

	//cleanup
	os.Remove("inventory_test.db")
}

func testGetDevice(t *testing.T) {
	repo := prepareTestDatabase()

	device, err := repo.GetDevice(1)

	assert.Nil(t, err)
	assert.Equal(t, expectedDevices[0], device)
}

func testGetDeviceError(t *testing.T) {
	var expectedDevice models.Device
	repo := prepareTestDatabase()

	device, err := repo.GetDevice(0)

	assert.Equal(t, err, fmt.Errorf("Couldn't get device item with id 0: sql: no rows in result set"))
	assert.Equal(t, &expectedDevice, device)
}

func testGetDevices(t *testing.T) {
	repo := prepareTestDatabase()

	devices, err := repo.GetDevices()

	assert.Nil(t, err)
	assert.Equal(t, expectedDevices, devices)
}

func testDeleteDevice(t *testing.T) {
	repo := prepareTestDatabase()

	err := repo.DeleteDevice(1)

	assert.Nil(t, err)
	assert.Equal(t, countRows(repo, "device"), 1)
}

func testCreateDevice(t *testing.T) {
	repo := prepareTestDatabase()

	device := expectedDevices[0]
	device.Name = "new device"
	id, err := repo.CreateDevice(device)

	assert.Nil(t, err)
	assert.Equal(t, 3, countRows(repo, "device"))
	assert.Equal(t, uint32(3), id)
}

func testUpdateDevice(t *testing.T) {
	repo := prepareTestDatabase()

	device := expectedDevices[0]
	device.Name = "new device"
	err := repo.UpdateDevice(1, device)

	assert.Nil(t, err)
}
