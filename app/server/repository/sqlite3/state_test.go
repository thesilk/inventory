package sqlite3

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	commonModel "gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/models"
)

var expectedStates []*models.State = []*models.State{
	{
		InventoryID: 1,
		UserID:      1,
		UpdateTime:  1,
	}, {
		InventoryID: 2,
		UserID:      2,
		UpdateTime:  2,
	},
}

var expectedInventory []*commonModel.Inventory = []*commonModel.Inventory{
	{
		ID:            1,
		Name:          "production",
		HostName:      "production.the.silk.host.com",
		IP:            "0.0.0.0",
		Port:          137,
		Version:       "v1.0.0",
		LongTermUsage: false,
		UserName:      "none",
		UserRole:      "none",
		UpdateTime:    1,
	},
	{
		ID:            2,
		Name:          "staging",
		HostName:      "staging.the.silk.host.com",
		IP:            "127.0.0.1",
		Port:          1337,
		Version:       "v1.0.1",
		LongTermUsage: true,
		UserName:      "peter",
		UserRole:      "software developer",
		UpdateTime:    2,
	},
}

func TestStateRepository(t *testing.T) {
	t.Run("get inventory item", testGetInventoryItem)
	t.Run("get inventory item error for non existing state entry", testGetInventoryError)
	t.Run("get inventory", testGetInventory)
	t.Run("delete state", testDeleteState)
	t.Run("create state", testCreateState)
	t.Run("update state", testUpdateState)

	//cleanup
	os.Remove("inventory_test.db")
}

func testGetInventoryItem(t *testing.T) {
	repo := prepareTestDatabase()

	inventoryItem, err := repo.GetInventoryItem(1)

	assert.Nil(t, err)
	assert.Equal(t, expectedInventory[0], inventoryItem)
}

func testGetInventoryError(t *testing.T) {
	var expectedInventoryItem commonModel.Inventory
	repo := prepareTestDatabase()

	inventoryItem, err := repo.GetInventoryItem(0)

	assert.Equal(t, err, fmt.Errorf("Couldn't get inventory item: sql: no rows in result set"))
	assert.Equal(t, &expectedInventoryItem, inventoryItem)
}

func testGetInventory(t *testing.T) {
	repo := prepareTestDatabase()

	inventory, err := repo.GetInventory()

	assert.Nil(t, err)
	assert.Equal(t, expectedInventory, inventory)
}

func testDeleteState(t *testing.T) {
	repo := prepareTestDatabase()

	err := repo.DeleteInventoryItem(1)

	assert.Nil(t, err)
	assert.Equal(t, countRows(repo, "state"), 1)
}

func testCreateState(t *testing.T) {
	repo := prepareTestDatabase()

	inventory := expectedStates[0]
	inventory.InventoryID = 3
	err := repo.CreateInventoryItem(inventory)

	assert.Nil(t, err)
	assert.Equal(t, countRows(repo, "state"), 3)
}

func testUpdateState(t *testing.T) {
	repo := prepareTestDatabase()

	inventory := expectedStates[0]
	inventory.UserID = 3
	inventory.UpdateTime = 3
	err := repo.UpdateInventoryItem(inventory.InventoryID, inventory.UserID, inventory.UpdateTime)

	assert.Nil(t, err)
}
