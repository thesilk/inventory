package sqlite3

import (
	"fmt"

	"gitlab.com/thesilk/inventory/app/models"
)

// GetUser creates user db entry
func (m *Repository) GetUser(id uint64) (*models.User, error) {
	var user models.User
	err := m.DB.QueryRow("SELECT * FROM user where id=$1", id).Scan(
		&user.ID,
		&user.Name,
		&user.Role,
	)
	if err != nil {
		return &user, fmt.Errorf("Couldn't get user with id %d: %v", id, err)
	}

	return &user, nil
}

// GetUsers returns all users
func (m *Repository) GetUsers() ([]*models.User, error) {
	var users []*models.User

	rows, err := m.DB.Query("SELECT * from user")
	if err != nil {
		return users, fmt.Errorf("Couldn't get user information: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var user models.User
		err := rows.Scan(
			&user.ID,
			&user.Name,
			&user.Role,
		)
		if err != nil {
			return []*models.User{}, fmt.Errorf("Couldn't scan user row: %v", err)
		}

		users = append(users, &user)
	}

	return users, nil
}

// CreateUser creates new user db entry
func (m *Repository) CreateUser(user *models.User) error {

	stmt, err := m.DB.Prepare(`INSERT INTO user (name, role) VALUES (?,?)`)

	if err != nil {
		return fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&user.Name,
		&user.Role,
	)
	if err != nil {
		return fmt.Errorf("Couldn't execute user insert statement: %v", err)
	}

	return nil
}

//UpdateUser updates given user
func (m *Repository) UpdateUser(id uint64, user *models.User) error {
	stmt, err := m.DB.Prepare(`
		UPDATE user SET
				name=?,
				role=? WHERE id=?`)

	if err != nil {
		return fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&user.Name,
		&user.Role,
		id,
	)
	if err != nil {
		return fmt.Errorf("Couldn't execute user update statement: %v", err)
	}

	return nil
}

//DeleteUser deletes given user
func (m *Repository) DeleteUser(id uint64) error {
	stmt, err := m.DB.Prepare("DELETE FROM user where id=?")
	if err != nil {
		return fmt.Errorf("Couldn't pepare statement for deleting user with id %d: %v", id, err)
	}

	_, err = stmt.Exec(id)
	if err != nil {
		return fmt.Errorf("Couldn't execute query for deleting user with id %d: %v", id, err)
	}

	return nil
}
