package sqlite3

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
)

var expectedUsers []*models.User = []*models.User{
	{
		ID:   1,
		Name: "none",
		Role: "none",
	}, {
		ID:   2,
		Name: "peter",
		Role: "software developer",
	}, {
		ID:   3,
		Name: "claudia",
		Role: "test engineer",
	},
}

func TestUserRepository(t *testing.T) {
	t.Run("get user", testGetUser)
	t.Run("get user error for non existing user", testGetUserError)
	t.Run("get users", testGetUsers)
	t.Run("delete user", testDeleteUser)
	t.Run("create user", testCreateUser)
	t.Run("update user", testUpdateUser)

	//cleanup
	os.Remove("inventory_test.db")
}

func testGetUser(t *testing.T) {
	repo := prepareTestDatabase()

	user, err := repo.GetUser(2)

	assert.Nil(t, err)
	assert.Equal(t, expectedUsers[1], user)
}

func testGetUserError(t *testing.T) {
	var expectedUser models.User
	repo := prepareTestDatabase()

	user, err := repo.GetUser(0)

	assert.Equal(t, err, fmt.Errorf("Couldn't get user with id 0: sql: no rows in result set"))
	assert.Equal(t, &expectedUser, user)
}

func testGetUsers(t *testing.T) {
	repo := prepareTestDatabase()

	users, err := repo.GetUsers()

	assert.Nil(t, err)
	assert.Equal(t, expectedUsers, users)
}

func testDeleteUser(t *testing.T) {
	repo := prepareTestDatabase()

	err := repo.DeleteUser(1)

	assert.Nil(t, err)
	assert.Equal(t, countRows(repo, "user"), 2)
}

func testCreateUser(t *testing.T) {
	repo := prepareTestDatabase()

	user := expectedUsers[0]
	user.Name = "new user"
	err := repo.CreateUser(user)

	assert.Nil(t, err)
	assert.Equal(t, countRows(repo, "user"), 4)
}

func testUpdateUser(t *testing.T) {
	repo := prepareTestDatabase()

	user := expectedUsers[0]
	user.Name = "new user"
	err := repo.UpdateUser(1, user)

	assert.Nil(t, err)
}
