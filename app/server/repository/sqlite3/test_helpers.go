package sqlite3

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/testfixtures.v2"
)

func prepareTestDatabase() *Repository {
	os.Remove("inventory_test.db")
	repo := NewTestRepository("../../migrations")

	fixtures, err := testfixtures.NewFiles(repo.DB, &testfixtures.SQLite{},
		"fixtures/device.yml",
		"fixtures/state.yml",
		"fixtures/user.yml",
	)
	if err != nil {
		log.Fatalf("couldn't detetct fixtures directory: %v", err)
	}

	if err := fixtures.Load(); err != nil {
		log.Fatalf("couldn't load fixture files: %v", err)
	}

	return repo
}

func countRows(repo *Repository, table string) (count int) {
	query := fmt.Sprintf("SELECT COUNT(*) AS count FROM %s", table)
	_ = repo.DB.QueryRow(query).Scan(&count)
	return count
}
