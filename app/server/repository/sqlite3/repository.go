package sqlite3

import (
	"database/sql"
	"fmt"
	"log"

	//"github.com/mattn/go-sqlite3" needs blank import
	_ "github.com/mattn/go-sqlite3"
	"github.com/pressly/goose"
)

// Repository struct to handle database connections
type Repository struct {
	DB *sql.DB
}

// DATABASEDRIVER is set to sqlite3 database
const DATABASEDRIVER = "sqlite3"

// NewRepository creates new repository instance
func NewRepository(migrationPath, databaseFile string) *Repository {
	var (
		r   Repository
		err error
	)

	r.DB, err = sql.Open(DATABASEDRIVER, databaseFile)
	if err != nil {
		panic(fmt.Sprintf("Couldn't open %s: %v", databaseFile, err))
	}

	if err := goose.SetDialect(DATABASEDRIVER); err != nil {
		panic(fmt.Sprintf("Couldn't set sqlite3 dialect: %v", err))
	}

	if err := goose.Up(r.DB, migrationPath); err != nil {
		panic(fmt.Sprintf("Couldn't migrate database: %v", err))
	}

	return &r
}

// NewTestRepository is only used fot testing purpose
func NewTestRepository(migrationPath string) *Repository {
	var (
		r   Repository
		err error
	)

	r.DB, err = sql.Open(DATABASEDRIVER, "inventory_test.db")
	if err != nil {
		log.Fatalf(fmt.Sprintf("Couldn't open inventory_test.db database: %v", err))
	}

	if err := goose.SetDialect(DATABASEDRIVER); err != nil {
		panic(fmt.Sprintf("Couldn't set sqlite3 dialect: %v", err))
	}

	if err := goose.Up(r.DB, migrationPath); err != nil {
		panic(fmt.Sprintf("Couldn't migrate database: %v", err))
	}

	return &r
}
