package interfaces

import (
	"fmt"

	commonModel "gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/models"
)

var inventory commonModel.Inventory = commonModel.Inventory{
	ID:         1,
	Name:       "test machine",
	HostName:   "host name",
	IP:         "0.0.0.0",
	Port:       1234,
	Version:    "v1",
	UserName:   "peter",
	UserRole:   "developer",
	UpdateTime: 0,
}

var _ InventoryPort = (*FakeInventory)(nil)

// FakeInventory provides fake struct to mock repository methods
type FakeInventory struct {
	HasError bool
}

// CreateInventoryItem mocks repository method
func (f FakeInventory) CreateInventoryItem(state *models.State) error {
	if f.HasError {
		return fmt.Errorf("couldn't create state")
	}
	return nil
}

// GetInventory mocks repository method
func (f FakeInventory) GetInventory() ([]*commonModel.Inventory, error) {
	if f.HasError {
		return []*commonModel.Inventory{}, fmt.Errorf("couldn't get inventory")
	}
	return []*commonModel.Inventory{&inventory}, nil
}

// GetInventoryItem mocks repository method
func (f FakeInventory) GetInventoryItem(id uint32) (*commonModel.Inventory, error) {
	var inventoryItem *commonModel.Inventory
	if f.HasError {
		return inventoryItem, fmt.Errorf("couldn't get inventory item")
	}
	return &inventory, nil
}

// UpdateInventoryItem mocks repository method
func (f FakeInventory) UpdateInventoryItem(id, userID, updateTime uint32) error {
	if f.HasError {
		return fmt.Errorf("couldn't update state")
	}
	return nil
}

// DeleteInventoryItem mocks repository method
func (f FakeInventory) DeleteInventoryItem(id uint32) error {
	if f.HasError {
		return fmt.Errorf("couldn't delete state")
	}
	return nil
}
