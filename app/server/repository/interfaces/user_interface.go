package interfaces

import "gitlab.com/thesilk/inventory/app/models"

// UserPort interface for repository methods
type UserPort interface {
	CreateUser(user *models.User) error
	GetUsers() ([]*models.User, error)
	GetUser(id uint64) (*models.User, error)
	UpdateUser(id uint64, user *models.User) error
	DeleteUser(id uint64) error
}
