package interfaces

import "gitlab.com/thesilk/inventory/app/models"

// DevicePort interface for repository methods
type DevicePort interface {
	CreateDevice(device *models.Device) (uint32, error)
	GetDevices() ([]*models.Device, error)
	GetDevice(id uint64) (*models.Device, error)
	UpdateDevice(id uint64, device *models.Device) error
	DeleteDevice(id uint64) error
}
