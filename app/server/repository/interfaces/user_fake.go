package interfaces

import (
	"fmt"

	"gitlab.com/thesilk/inventory/app/models"
)

var user models.User = models.User{
	ID:   1,
	Name: "Test User",
	Role: "dev",
}

var _ UserPort = (*FakeUser)(nil)

// FakeUser provides struct to mock repository methods
type FakeUser struct {
	HasError bool
}

// CreateUser mocks repository method
func (f FakeUser) CreateUser(inventory *models.User) error {
	if f.HasError {
		return fmt.Errorf("couldn't create user")
	}
	return nil
}

// GetUsers mocks repository method
func (f FakeUser) GetUsers() ([]*models.User, error) {
	if f.HasError {
		return []*models.User{}, fmt.Errorf("couldn't get users")
	}
	return []*models.User{&user}, nil
}

// GetUser mocks repository method
func (f FakeUser) GetUser(id uint64) (*models.User, error) {
	var inventory *models.User
	if f.HasError {
		return inventory, fmt.Errorf("couldn't get user")
	}
	return &user, nil
}

// UpdateUser mocks repository method
func (f FakeUser) UpdateUser(id uint64, user *models.User) error {
	if f.HasError {
		return fmt.Errorf("couldn't update user")
	}
	return nil
}

// DeleteUser mocks repository method
func (f FakeUser) DeleteUser(id uint64) error {
	if f.HasError {
		return fmt.Errorf("couldn't delete user")
	}
	return nil
}
