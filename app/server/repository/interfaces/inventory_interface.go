package interfaces

import (
	commonModel "gitlab.com/thesilk/inventory/app/models"
	"gitlab.com/thesilk/inventory/app/server/models"
)

// InventoryPort interface for repository methods
type InventoryPort interface {
	GetInventory() ([]*commonModel.Inventory, error)
	GetInventoryItem(id uint32) (*commonModel.Inventory, error)
	UpdateInventoryItem(id, userID, updateTime uint32) error
	CreateInventoryItem(state *models.State) error
	DeleteInventoryItem(id uint32) error
}
