package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/thesilk/inventory/app/cli/cmd"
	repository "gitlab.com/thesilk/inventory/app/cli/repository/api"
	"gitlab.com/thesilk/inventory/app/cli/usecase"
	"gitlab.com/thesilk/inventory/app/config"
	"gitlab.com/thesilk/inventory/app/models"
)

var (
	version string
)

func logErrorAndExit(err error) {
	log.Println(err)
	os.Exit(1)
}

func main() {
	initConfig()

	updateInventory := cmd.NewUpdateInventory()
	createUser := cmd.NewCreateUser()
	deleteUser := cmd.NewDeleteUser()
	updateUser := cmd.NewUpdateUser()
	setConfig := cmd.NewSetConfig()
	showInventoryCommand := flag.NewFlagSet("show-inventory", flag.ExitOnError)
	showUsersCommand := flag.NewFlagSet("show-users", flag.ExitOnError)
	getConfigCommand := flag.NewFlagSet("get-config", flag.ExitOnError)
	helpFlag := flag.Bool("help", false, "help")
	versionFlag := flag.Bool("version", false, "version")
	flag.Parse()

	if *helpFlag {
		printHelp()
	}

	if *versionFlag {
		fmt.Println(version)
		os.Exit(0)
	}

	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "create-user":
			if err := createUser.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "delete-user":
			if err := deleteUser.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "show-inventory":
			if err := showInventoryCommand.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "show-users":
			if err := showUsersCommand.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "update-inventory":
			if err := updateInventory.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "update-user":
			if err := updateUser.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "get-config":
			if err := getConfigCommand.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "set-config":
			if err := setConfig.Parse(os.Args[2:]); err != nil {
				logErrorAndExit(err)
			}
		case "help":
			printHelp()
		default:
			logErrorAndExit(fmt.Errorf("couldn't find sub-command"))
		}

	} else {
		printHelp()
	}

	api := repository.NewAPI(&http.Client{}, config.Cfg.GetServerHost(), config.Cfg.GetServerPort())
	usecaseInventory := usecase.NewInventoryUsecase(api)
	usecaseUser := usecase.NewUserUsecase(api)

	if createUser.Cmd.Parsed() {
		var user models.User = models.User{Name: createUser.UserName, Role: createUser.UserRole}
		err := usecaseUser.CreateUser(&user)
		if err != nil {
			logErrorAndExit(err)
		}
	}

	if deleteUser.Cmd.Parsed() {
		err := usecaseUser.DeleteUser(deleteUser.ID)
		if err != nil {
			logErrorAndExit(err)
		}
	}

	if showInventoryCommand.Parsed() {
		if err := usecaseInventory.PrintInventoryTable(); err != nil {
			logErrorAndExit(err)
		}
	}

	if showUsersCommand.Parsed() {
		if err := usecaseUser.PrintUserTable(); err != nil {
			logErrorAndExit(err)
		}
	}

	if updateInventory.Cmd.Parsed() {
		err := usecaseInventory.UpdateInventoryItem(updateInventory.ID, updateInventory.UserID)
		if err != nil {
			logErrorAndExit(err)
		}
	}

	if updateUser.Cmd.Parsed() {
		var user models.User = models.User{Name: updateUser.UserName, Role: updateUser.UserRole}
		err := usecaseUser.UpdateUser(updateUser.ID, &user)
		if err != nil {
			logErrorAndExit(err)
		}
	}

	if getConfigCommand.Parsed() {
		fmt.Printf("Host:\t%s\n", config.Cfg.GetServerHost())
		fmt.Printf("Port:\t%d\n", config.Cfg.GetServerPort())
	}

	if setConfig.Cmd.Parsed() {
		if setConfig.Host != "" {
			config.Cfg.SetServerHost(setConfig.Host)
		}
		if setConfig.Port != 0 {
			config.Cfg.SetServerPort(setConfig.Port)
		}
		if err := config.WriteConfig(getConfigPath()); err != nil {
			logErrorAndExit(err)
		}
	}
}

func getConfigPath() string {
	home, err := os.UserHomeDir()
	if err != nil {
		logErrorAndExit(err)
	}

	return filepath.Join(home, ".config/inventory/config.yaml")
}

func initConfig() {
	if err := config.InitConfig(getConfigPath()); err != nil {
		logErrorAndExit(err)
	}
}

func printHelp() {
	fmt.Println("Command-line tool to communicate with inventory-backend service to manage user-device relationships")
	fmt.Println("")
	fmt.Println("Subcommands:")
	fmt.Println("\tcreate-user")
	fmt.Println("\t\tcreates new user")
	fmt.Println("\tdelete-user")
	fmt.Println("\t\tdeletes existing user")
	fmt.Println("\tshow-inventory")
	fmt.Println("\t\tprints table with all user-device relationships")
	fmt.Println("\tshow-users")
	fmt.Println("\t\tprints table with all users")
	fmt.Println("\tupdate-inventory")
	fmt.Println("\t\tset/reset user of given device")
	fmt.Println("\tupdate-user")
	fmt.Println("\t\tupdate given user (all properties must be given for no empty overriding)")
	fmt.Println("\tget-config")
	fmt.Println("\t\tprints current config parameter")
	fmt.Println("\tset-config")
	fmt.Println("\t\toverrides config parameter")
	fmt.Println("\thelp")
	fmt.Println("\t\tprints help")
	os.Exit(0)
}
