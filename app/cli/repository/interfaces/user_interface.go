package interfaces

import "gitlab.com/thesilk/inventory/app/models"

// UserPort interface for repository methods
type UserPort interface {
	CreateUser(user *models.User) error
	GetUsers() ([]*models.User, error)
	GetUser(id uint32) (*models.User, error)
	UpdateUser(id uint32, user *models.User) error
	DeleteUser(id uint32) error
}
