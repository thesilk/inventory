package interfaces

import (
	"fmt"

	"gitlab.com/thesilk/inventory/app/models"
)

var device models.Device = models.Device{
	ID:       1,
	Name:     "Test Device",
	HostName: "Local Horst",
	IP:       "0.0.0.0",
	Port:     1337,
	Version:  "v2.1.6",
}

var _ DevicePort = (*FakeDevice)(nil)

// FakeDevice provides fake method
type FakeDevice struct {
	HasError bool
}

// CreateDevice provides fake method
func (f FakeDevice) CreateDevice(inventory *models.Device) (uint32, error) {
	if f.HasError {
		return 0, fmt.Errorf("couldn't create device")
	}
	return 1, nil
}

// GetDevices provides fake method
func (f FakeDevice) GetDevices() ([]*models.Device, error) {
	if f.HasError {
		return []*models.Device{}, fmt.Errorf("couldn't get devices")
	}
	return []*models.Device{&device}, nil
}

// GetDevice provides fake method
func (f FakeDevice) GetDevice(id uint64) (*models.Device, error) {
	var inventory *models.Device
	if f.HasError {
		return inventory, fmt.Errorf("couldn't get device")
	}
	return &device, nil
}

// UpdateDevice provides fake method
func (f FakeDevice) UpdateDevice(id uint64, device *models.Device) error {
	if f.HasError {
		return fmt.Errorf("couldn't update device")
	}
	return nil
}

// DeleteDevice provides fake method
func (f FakeDevice) DeleteDevice(id uint64) error {
	if f.HasError {
		return fmt.Errorf("couldn't delete device")
	}
	return nil
}
