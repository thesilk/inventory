package interfaces

import (
	"fmt"

	"gitlab.com/thesilk/inventory/app/models"
)

var user models.User = models.User{
	ID:   1,
	Name: "Test User",
	Role: "dev",
}

var _ UserPort = (*FakeUser)(nil)

// FakeUser provides struct to mock repository methods
type FakeUser struct {
	HasError bool
}

// CreateUser mocks repository method
func (f FakeUser) CreateUser(user *models.User) error {
	if f.HasError {
		return fmt.Errorf("couldn't create user")
	}
	return nil
}

// GetUsers mocks repository method
func (f FakeUser) GetUsers() ([]*models.User, error) {
	if f.HasError {
		return []*models.User{}, fmt.Errorf("couldn't get users")
	}
	return []*models.User{&user}, nil
}

// GetUser mocks repository method
func (f FakeUser) GetUser(id uint32) (*models.User, error) {
	var emptyUser *models.User
	if f.HasError {
		return emptyUser, fmt.Errorf("couldn't get user with id %d", id)
	}
	return &user, nil
}

// UpdateUser mocks repository method
func (f FakeUser) UpdateUser(id uint32, user *models.User) error {
	if f.HasError {
		return fmt.Errorf("couldn't update user")
	}
	return nil
}

// DeleteUser mocks repository method
func (f FakeUser) DeleteUser(id uint32) error {
	if f.HasError {
		return fmt.Errorf("couldn't delete user")
	}
	return nil
}
