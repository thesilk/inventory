package interfaces

import (
	commonModel "gitlab.com/thesilk/inventory/app/models"
)

// InventoryPort interface for repository methods
type InventoryPort interface {
	GetInventory() ([]*commonModel.Inventory, error)
	UpdateInventoryItem(id, userID uint32) error
}
