package interfaces

import (
	"fmt"

	commonModel "gitlab.com/thesilk/inventory/app/models"
)

var inventory commonModel.Inventory = commonModel.Inventory{
	ID:         1,
	Name:       "test machine",
	HostName:   "host name",
	IP:         "0.0.0.0",
	Port:       1234,
	Version:    "v1",
	UserName:   "peter",
	UserRole:   "developer",
	UpdateTime: 0,
}

var _ InventoryPort = (*FakeInventory)(nil)

// FakeInventory provides fake struct to mock repository methods
type FakeInventory struct {
	HasError bool
}

// GetInventory mocks repository method
func (f FakeInventory) GetInventory() ([]*commonModel.Inventory, error) {
	if f.HasError {
		return []*commonModel.Inventory{}, fmt.Errorf("couldn't get inventory")
	}
	return []*commonModel.Inventory{&inventory}, nil
}

// UpdateInventoryItem mocks repository method
func (f FakeInventory) UpdateInventoryItem(id, userID uint32) error {
	if f.HasError {
		return fmt.Errorf("couldn't update state")
	}
	return nil
}
