package repository

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"testing"
)

// MockClient is the mock client
type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

var (
	// GetDoFunc fetches the mock client's `Do` func
	GetDoFunc  func(req *http.Request) (*http.Response, error)
	mockClient MockClient = MockClient{}
)

// Do is the mock client's `Do` func
func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return GetDoFunc(req)
}

func structToReader(t *testing.T, payload interface{}) io.ReadCloser {
	payloadBytes, err := json.Marshal(&payload)
	if err != nil {
		t.Fatal(err)
	}
	return ioutil.NopCloser(bytes.NewReader(payloadBytes))

}
