package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/thesilk/inventory/app/models"
)

// GetInventory returns all inventory items after requesting backend
func (api *API) GetInventory() ([]*models.Inventory, error) {
	var (
		inventory []*models.Inventory
		errResp   ErrResponse
	)

	searchURL := fmt.Sprintf("%sinventory", api.URL)

	req, err := http.NewRequest(http.MethodGet, searchURL, io.LimitReader(strings.NewReader(""), 256))
	if err != nil {
		return inventory, fmt.Errorf("couldn't create get request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return inventory, fmt.Errorf("couldn't get inventory from backend: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return inventory, fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode == http.StatusOK {
		if err := json.Unmarshal(body, &inventory); err != nil {
			return inventory, fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}

	} else {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return inventory, fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return inventory, fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return inventory, nil
}

// UpdateInventoryItem update user device relationship
func (api *API) UpdateInventoryItem(id, userID uint32) error {
	var errResp ErrResponse

	searchURL := fmt.Sprintf("%sinventory/%d?userId=%d", api.URL, id, userID)

	req, err := http.NewRequest(http.MethodPut, searchURL, io.LimitReader(strings.NewReader(""), 256))
	if err != nil {
		return fmt.Errorf("couldn't create put request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("couldn't get inventory item from backend: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode != http.StatusNoContent {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return nil
}
