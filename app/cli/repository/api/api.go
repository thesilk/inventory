package repository

import (
	"fmt"
	"net/http"
)

// API struct to handle backend calls
type API struct {
	URL        string
	HTTPClient HTTPClient
}

// ErrResponse receives api error
type ErrResponse struct {
	Error string `json:"error"`
}

// HTTPClient interface
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// NewAPI creates api instance
func NewAPI(client HTTPClient, host string, port int) *API {
	url := fmt.Sprintf("http://%s:%d/api/v0/", host, port)
	api := API{URL: url, HTTPClient: client}
	return &api
}
