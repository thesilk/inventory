package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/thesilk/inventory/app/models"
)

// GetUsers returns all users after requesting backend
func (api *API) GetUsers() ([]*models.User, error) {
	var (
		users   []*models.User
		errResp ErrResponse
	)

	searchURL := fmt.Sprintf("%susers", api.URL)

	req, err := http.NewRequest(http.MethodGet, searchURL, io.LimitReader(strings.NewReader(""), 256))
	if err != nil {
		return users, fmt.Errorf("couldn't create get request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return users, fmt.Errorf("couldn't get inventory from backend: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return users, fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode == http.StatusOK {
		if err := json.Unmarshal(body, &users); err != nil {
			return users, fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}

	} else {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return users, fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return users, fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return users, nil
}

// GetUser returns user with given id
func (api *API) GetUser(id uint32) (*models.User, error) {
	var (
		user    *models.User
		errResp ErrResponse
	)

	searchURL := fmt.Sprintf("%suser/%d", api.URL, id)

	req, err := http.NewRequest(http.MethodGet, searchURL, io.LimitReader(strings.NewReader(""), 256))
	if err != nil {
		return user, fmt.Errorf("couldn't create get request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return user, fmt.Errorf("couldn't get user with id %d: %v", id, err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return user, fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode == http.StatusOK {
		if err := json.Unmarshal(body, &user); err != nil {
			return user, fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
	} else {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return user, fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return user, fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return user, nil
}

// UpdateUser updates user
func (api *API) UpdateUser(id uint32, user *models.User) error {
	var errResp ErrResponse

	searchURL := fmt.Sprintf("%suser/%d", api.URL, id)

	payload, err := json.Marshal(&user)
	if err != nil {
		return fmt.Errorf("couldn't serialize user: %v", err)
	}

	req, err := http.NewRequest(http.MethodPut, searchURL, bytes.NewReader(payload))
	if err != nil {
		return fmt.Errorf("couldn't create put request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("couldn't update user: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return nil
}

// CreateUser creates new user
func (api *API) CreateUser(user *models.User) error {
	var errResp ErrResponse

	searchURL := fmt.Sprintf("%susers", api.URL)

	payload, err := json.Marshal(&user)
	if err != nil {
		return fmt.Errorf("couldn't serialize user: %v", err)
	}

	req, err := http.NewRequest(http.MethodPost, searchURL, bytes.NewReader(payload))
	if err != nil {
		return fmt.Errorf("couldn't create post request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("couldn't create user: %v", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode != http.StatusCreated {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return nil
}

// DeleteUser deletes given user
func (api *API) DeleteUser(id uint32) error {
	var errResp ErrResponse

	searchURL := fmt.Sprintf("%suser/%d", api.URL, id)

	req, err := http.NewRequest(http.MethodDelete, searchURL, io.LimitReader(strings.NewReader(""), 256))
	if err != nil {
		return fmt.Errorf("couldn't create delete request: %v", err)
	}

	resp, err := api.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("couldn't delete user with id %d: %v", id, err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, 1048576))
	if err != nil {
		return fmt.Errorf("couldn't read response body from backend service: %v", err)
	}

	if resp.StatusCode != http.StatusNoContent {
		if err := json.Unmarshal(body, &errResp); err != nil {
			return fmt.Errorf("couldn't deserialize response of backend service: %v", err)
		}
		return fmt.Errorf("%d: %v", resp.StatusCode, errResp.Error)
	}

	return nil
}
