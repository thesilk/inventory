package repository

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
)

func TestUserRequests(t *testing.T) {
	t.Run("get users", testGetUsers)
	t.Run("get users responses with internal server error", testGetUsersInternalError)
	t.Run("get user", testGetUser)
	t.Run("get user responses with internal server error", testGetUserInternalError)
	t.Run("update given user", testUpdateUser)
	t.Run("update given user responses with internal server error", testUpdateUserError)
	t.Run("delete given user", testDeleteUser)
	t.Run("delete given user responses with internal server error", testDeleteUserError)
	t.Run("create given user", testCreateUser)
	t.Run("create given user responses with internal server error", testCreateUserError)
}

func testGetUsers(t *testing.T) {
	var expectedUsers []*models.User = []*models.User{{ID: 1, Name: "Peter", Role: "QA"}}

	payloadReader := structToReader(t, expectedUsers)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	actualUsers, err := api.GetUsers()

	assert.Nil(t, err)
	assert.Equal(t, expectedUsers, actualUsers)
}

func testGetUsersInternalError(t *testing.T) {
	var (
		errMessage     string      = "internal server error"
		errResp        ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg string      = "500: " + errMessage
		expectedUsers  []*models.User
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	actualUsers, err := api.GetUsers()

	assert.Equal(t, expectedErrMsg, err.Error())
	assert.Equal(t, expectedUsers, actualUsers)
}

func testGetUser(t *testing.T) {
	var expectedUser models.User = models.User{ID: 1, Name: "Peter", Role: "QA"}

	payloadReader := structToReader(t, expectedUser)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	actualUser, err := api.GetUser(uint32(1))

	assert.Nil(t, err)
	assert.Equal(t, &expectedUser, actualUser)
}

func testGetUserInternalError(t *testing.T) {
	var (
		errMessage     string      = "internal server error"
		errResp        ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg string      = "500: " + errMessage
		expectedUser   *models.User
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	actualUser, err := api.GetUser(uint32(1))

	assert.Equal(t, expectedErrMsg, err.Error())
	assert.Equal(t, expectedUser, actualUser)
}

func testUpdateUser(t *testing.T) {
	var expectedUser models.User = models.User{ID: 1, Name: "Peter", Role: "QA"}

	payloadReader := structToReader(t, expectedUser)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.UpdateUser(uint32(1), &expectedUser)

	assert.Nil(t, err)
}

func testUpdateUserError(t *testing.T) {
	var (
		errMessage     string      = "internal server error"
		errResp        ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg string      = "500: " + errMessage
		expectedUser   models.User = models.User{ID: 1, Name: "Peter", Role: "QA"}
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.UpdateUser(uint32(1), &expectedUser)

	assert.Equal(t, expectedErrMsg, err.Error())
}

func testDeleteUser(t *testing.T) {
	payloadReader := structToReader(t, nil)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusNoContent,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.DeleteUser(uint32(1))

	assert.Nil(t, err)
}

func testDeleteUserError(t *testing.T) {
	var (
		errMessage     string      = "internal server error"
		errResp        ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg string      = "500: " + errMessage
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.DeleteUser(uint32(1))

	assert.Equal(t, expectedErrMsg, err.Error())
}

func testCreateUser(t *testing.T) {
	var expectedUser models.User = models.User{ID: 1, Name: "Peter", Role: "QA"}

	payloadReader := structToReader(t, expectedUser)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.CreateUser(&expectedUser)

	assert.Nil(t, err)
}

func testCreateUserError(t *testing.T) {
	var (
		errMessage     string      = "internal server error"
		errResp        ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg string      = "500: " + errMessage
		expectedUser   models.User = models.User{ID: 1, Name: "Peter", Role: "QA"}
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.CreateUser(&expectedUser)

	assert.Equal(t, expectedErrMsg, err.Error())
}
