package repository

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/models"
)

func TestInventoryRequests(t *testing.T) {
	t.Run("get inventory", testGetInventory)
	t.Run("get inventory responses with internal server error", testGetInventoryInternalError)
	t.Run("update inventory item (user-device relationship)", testUpdateInventory)
	t.Run("update inventory item responses with internal server error", testUpdateInventoryError)
}

func testGetInventory(t *testing.T) {
	var expectedInventory []*models.Inventory = []*models.Inventory{{Name: "test", IP: "0.0.0.0", UserName: "Peter"}}

	payloadReader := structToReader(t, expectedInventory)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	actualInventory, err := api.GetInventory()

	assert.Nil(t, err)
	assert.Equal(t, expectedInventory, actualInventory)
}

func testGetInventoryInternalError(t *testing.T) {
	var (
		errMessage        string      = "internal server error"
		errResp           ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg    string      = "500: " + errMessage
		expectedInventory []*models.Inventory
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	actualInventory, err := api.GetInventory()

	assert.Equal(t, expectedErrMsg, err.Error())
	assert.Equal(t, expectedInventory, actualInventory)
}

func testUpdateInventory(t *testing.T) {
	payloadReader := structToReader(t, nil)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusNoContent,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.UpdateInventoryItem(uint32(1), uint32(2))

	assert.Nil(t, err)
}

func testUpdateInventoryError(t *testing.T) {
	var (
		errMessage     string      = "internal server error"
		errResp        ErrResponse = ErrResponse{Error: errMessage}
		expectedErrMsg string      = "500: " + errMessage
	)

	payloadReader := structToReader(t, errResp)

	GetDoFunc = func(*http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       payloadReader,
		}, nil
	}

	api := NewAPI(&mockClient, "localhost", 1337)

	err := api.UpdateInventoryItem(uint32(1), uint32(2))

	assert.Equal(t, expectedErrMsg, err.Error())
}
