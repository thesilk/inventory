package usecase

import (
	"os"

	"github.com/jedib0t/go-pretty/table"
	"gitlab.com/thesilk/inventory/app/cli/repository/interfaces"
	"gitlab.com/thesilk/inventory/app/models"
)

// InventoryUsecase handles inventory domain buisness logic
type InventoryUsecase struct {
	api interfaces.InventoryPort
}

// NewInventoryUsecase creates device usecase struct
func NewInventoryUsecase(api interfaces.InventoryPort) InventoryUsecase {
	return InventoryUsecase{api: api}
}

// GetInventory calls request to backend to get the whole inventory
func (u InventoryUsecase) PrintInventoryTable() error {
	inventory, err := u.api.GetInventory()
	if err != nil {
		return err
	}

	printInventoryTable(inventory)

	return nil
}

// UpdateInventoryItem updates user device relationship
func (u InventoryUsecase) UpdateInventoryItem(id, userID uint32) error {
	return u.api.UpdateInventoryItem(id, userID)
}

func printInventoryTable(inventory []*models.Inventory) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Name", "Hostame", "IP", "Port", "Version", "User Name", "User Role", "Update Time", "Long Term Usage"})
	for _, item := range inventory {
		t.AppendRow(table.Row{item.ID, item.Name, item.HostName, item.IP, item.Port, item.Version, item.UserName, item.UserRole, item.UpdateTime, item.LongTermUsage})

	}
	t.Render()
}
