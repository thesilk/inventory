package usecase

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/cli/repository/interfaces"
)

func TestInventoryUsecase(t *testing.T) {
	t.Run("print inventory table", testPrintInventoryTable)
	t.Run("print inventory table with error", testPrintInventoryTableError)
	t.Run("update inventoryItem", testUpdateInventoryItem)
	t.Run("update inventoryItem returns error", testUpdateInventoryItemError)
}

func testPrintInventoryTable(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	err := u.PrintInventoryTable()
	assert.Nil(t, err)
}

func testPrintInventoryTableError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	err := u.PrintInventoryTable()
	assert.Equal(t, fmt.Errorf("couldn't get inventory"), err)
}

func testUpdateInventoryItem(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{}
	u := NewInventoryUsecase(fakeAPI)

	err := u.UpdateInventoryItem(uint32(1), uint32(1))
	assert.Nil(t, err)
}

func testUpdateInventoryItemError(t *testing.T) {
	fakeAPI := interfaces.FakeInventory{HasError: true}
	u := NewInventoryUsecase(fakeAPI)

	err := u.UpdateInventoryItem(uint32(1), uint32(1))
	assert.Equal(t, fmt.Errorf("couldn't update state"), err)
}
