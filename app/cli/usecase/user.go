package usecase

import (
	"os"

	"github.com/jedib0t/go-pretty/table"
	"gitlab.com/thesilk/inventory/app/cli/repository/interfaces"
	"gitlab.com/thesilk/inventory/app/models"
)

// UserUsecase handles user domain buisness logic
type UserUsecase struct {
	api interfaces.UserPort
}

// NewUserUsecase creates user usecase struct
func NewUserUsecase(api interfaces.UserPort) UserUsecase {
	return UserUsecase{api: api}
}

// PrintUserTable calls request to backend to print all users
func (u UserUsecase) PrintUserTable() error {
	users, err := u.api.GetUsers()
	if err != nil {
		return err
	}

	printUsersTable(users)

	return nil
}

// UpdateUser updates user
func (u UserUsecase) UpdateUser(id uint32, user *models.User) error {
	oldUser, err := u.api.GetUser(id)
	if err != nil {
		return err
	}

	if user.Name == "" {
		user.Name = oldUser.Name
	}

	if user.Role == "" {
		user.Role = oldUser.Role
	}

	return u.api.UpdateUser(id, user)
}

// CreateUser updates user
func (u UserUsecase) CreateUser(user *models.User) error {
	return u.api.CreateUser(user)
}

// DeleteUser updates user
func (u UserUsecase) DeleteUser(id uint32) error {
	return u.api.DeleteUser(id)
}

func printUsersTable(users []*models.User) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Name", "Role"})
	for _, item := range users {
		t.AppendRow(table.Row{item.ID, item.Name, item.Role})

	}
	t.Render()
}
