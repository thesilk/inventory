package usecase

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thesilk/inventory/app/cli/repository/interfaces"
	"gitlab.com/thesilk/inventory/app/models"
)

var user models.User = models.User{ID: 2, Name: "Peter", Role: "QA"}

func TestUserUsecase(t *testing.T) {
	t.Run("print user table", testPrintUserTable)
	t.Run("print user table with error", testPrintUserTableError)
	t.Run("update user", testUpdateUser)
	t.Run("update user returns error", testUpdateUserError)
	t.Run("update user with missing user name", testUpdateUserMissingUserName)
	t.Run("update user with missung user role", testUpdateUserMissingUserRole)
	t.Run("delete user", testDeleteUser)
	t.Run("delete user returns error", testDeleteUserError)
	t.Run("create user", testCreateUser)
	t.Run("create user returns error", testCreateUserError)
}

func testPrintUserTable(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.PrintUserTable()
	assert.Nil(t, err)
}

func testPrintUserTableError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	err := u.PrintUserTable()
	assert.Equal(t, fmt.Errorf("couldn't get users"), err)
}

func testUpdateUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.UpdateUser(uint32(1), &user)
	assert.Nil(t, err)
}

func testUpdateUserError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	err := u.UpdateUser(uint32(1), &user)
	assert.Equal(t, fmt.Errorf("couldn't get user with id 1"), err)
}

func testUpdateUserMissingUserName(t *testing.T) {
	var newUser models.User = models.User{ID: 1, Role: "Applicant"}
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.UpdateUser(uint32(1), &newUser)
	assert.Nil(t, err)
}

func testUpdateUserMissingUserRole(t *testing.T) {
	var newUser models.User = models.User{ID: 1, Name: "Peter"}
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.UpdateUser(uint32(1), &newUser)
	assert.Nil(t, err)
}

func testDeleteUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.DeleteUser(uint32(1))
	assert.Nil(t, err)
}

func testDeleteUserError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	err := u.DeleteUser(uint32(1))
	assert.Equal(t, fmt.Errorf("couldn't delete user"), err)
}

func testCreateUser(t *testing.T) {
	fakeAPI := interfaces.FakeUser{}
	u := NewUserUsecase(fakeAPI)

	err := u.CreateUser(&user)
	assert.Nil(t, err)
}

func testCreateUserError(t *testing.T) {
	fakeAPI := interfaces.FakeUser{HasError: true}
	u := NewUserUsecase(fakeAPI)

	err := u.CreateUser(&user)
	assert.Equal(t, fmt.Errorf("couldn't create user"), err)
}
