package cmd

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateUserCommand(t *testing.T) {
	t.Run("creating new struct", testCreateUserCommand)
	t.Run("parse with given arguments", testCreateUserParsing)
	t.Run("parse with no given user name", testCreateUserParsingNoUserNameGiven)
}

func testCreateUserCommand(t *testing.T) {
	createUser := NewCreateUser()

	assert.Equal(t, "", createUser.UserName)
	assert.Equal(t, "", createUser.UserRole)
}

func testCreateUserParsing(t *testing.T) {
	createUser := NewCreateUser()

	err := createUser.Parse([]string{"--user-name", "bart", "--user-role", "test engineer"})

	assert.Nil(t, err)
	assert.Equal(t, "bart", createUser.UserName)
	assert.Equal(t, "test engineer", createUser.UserRole)

}

func testCreateUserParsingNoUserNameGiven(t *testing.T) {
	createUser := NewCreateUser()

	err := createUser.Parse([]string{})

	assert.Equal(t, fmt.Errorf("user-name must be specified"), err)
}
