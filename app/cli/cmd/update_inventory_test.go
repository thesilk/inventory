package cmd

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUpdateInventoryCommand(t *testing.T) {
	t.Run("creating new struct", testCommand)
	t.Run("parse with given arguments", testParsing)
	t.Run("parse with no given device id", testParsingNoDeviceId)
	t.Run("parse arguments to reset user (no given user-id argument)", testParsingResetUser)
}

func testCommand(t *testing.T) {
	updateInventory := NewUpdateInventory()

	assert.Equal(t, uint32(0), updateInventory.ID)
	assert.Equal(t, uint32(0), updateInventory.UserID)
}

func testParsing(t *testing.T) {
	updateInventory := NewUpdateInventory()

	err := updateInventory.Parse([]string{"--id", "3", "--user-id", "2"})

	assert.Nil(t, err)
	assert.Equal(t, uint32(3), updateInventory.ID)
	assert.Equal(t, uint32(2), updateInventory.UserID)

}

func testParsingNoDeviceId(t *testing.T) {
	updateInventory := NewUpdateInventory()

	err := updateInventory.Parse([]string{})

	assert.Equal(t, fmt.Errorf("couldn't update user because of missing device id"), err)
}

func testParsingResetUser(t *testing.T) {
	updateInventory := NewUpdateInventory()

	err := updateInventory.Parse([]string{"--id", "3"})

	assert.Nil(t, err)
	assert.Equal(t, uint32(3), updateInventory.ID)
	assert.Equal(t, uint32(1), updateInventory.UserID)
}
