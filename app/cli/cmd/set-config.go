package cmd

import (
	"flag"
	"fmt"
)

//SetConfig struct to handle flags
type SetConfig struct {
	Cmd  *flag.FlagSet
	Host string
	Port int
}

var (
	host *string
	port *int
)

// NewSetConfig creates SetConfig flag instance
func NewSetConfig() *SetConfig {
	var cfg SetConfig

	cfg.Cmd = flag.NewFlagSet("set-config", flag.ExitOnError)
	host = cfg.Cmd.String("host", "", "server host address")
	port = cfg.Cmd.Int("port", 0, "server port")

	return &cfg
}

// Parse subcommand flags
func (cfg *SetConfig) Parse(args []string) error {
	err := cfg.Cmd.Parse(args)
	if err != nil {
		return fmt.Errorf("couldn't parse flags for 'set-config' command: %v", err)
	}

	cfg.Host = *host
	cfg.Port = *port

	return nil
}
