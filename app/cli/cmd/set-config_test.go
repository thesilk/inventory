package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetConfigCommand(t *testing.T) {
	t.Run("creating new struct", testSetConfigCommand)
	t.Run("parse with given arguments", testSetConfigParsing)
}

func testSetConfigCommand(t *testing.T) {
	cfg := NewSetConfig()

	assert.Equal(t, "", cfg.Host)
	assert.Equal(t, 0, cfg.Port)
}

func testSetConfigParsing(t *testing.T) {
	cfg := NewSetConfig()

	err := cfg.Parse([]string{"--host", "0.0.0.0", "--port", "1337"})

	assert.Nil(t, err)
	assert.Equal(t, "0.0.0.0", cfg.Host)
	assert.Equal(t, 1337, cfg.Port)

}
