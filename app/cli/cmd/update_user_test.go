package cmd

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUpdateUseryCommand(t *testing.T) {
	t.Run("creating new struct", testUpdateUserCommand)
	t.Run("parse with given arguments", testUpdateUserParsing)
	t.Run("parse with no given device id", testUpdateUserParsingNoUserID)
}

func testUpdateUserCommand(t *testing.T) {
	updateUser := NewUpdateUser()

	assert.Equal(t, uint32(0), updateUser.ID)
	assert.Equal(t, "", updateUser.UserName)
	assert.Equal(t, "", updateUser.UserRole)
}

func testUpdateUserParsing(t *testing.T) {
	updateUser := NewUpdateUser()

	err := updateUser.Parse([]string{"--id", "3", "--user-name", "Sven", "--user-role", "Software Developer"})

	assert.Nil(t, err)
	assert.Equal(t, uint32(3), updateUser.ID)
	assert.Equal(t, "Sven", updateUser.UserName)
	assert.Equal(t, "Software Developer", updateUser.UserRole)

}

func testUpdateUserParsingNoUserID(t *testing.T) {
	updateUser := NewUpdateUser()

	err := updateUser.Parse([]string{})

	assert.Equal(t, fmt.Errorf("couldn't update user because of missing user id"), err)
}
