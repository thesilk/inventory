package cmd

import (
	"flag"
	"fmt"
)

//UpdateInventory struct to handle flags
type UpdateInventory struct {
	Cmd        *flag.FlagSet
	ID, UserID uint32
}

var id, userID *uint64

// NewUpdateInventory creates UpdateInventory flag instance
func NewUpdateInventory() *UpdateInventory {
	var updateInventory UpdateInventory

	updateInventory.Cmd = flag.NewFlagSet("update-inventory", flag.ExitOnError)
	id = updateInventory.Cmd.Uint64("id", 0, "inventory id")
	userID = updateInventory.Cmd.Uint64("user-id", 1, "user id")

	return &updateInventory
}

// Parse subcommand flags
func (updateInventory *UpdateInventory) Parse(args []string) error {
	err := updateInventory.Cmd.Parse(args)
	if err != nil {
		return fmt.Errorf("couldn't parse flags for 'update-inventory' command: %v", err)
	}

	if *id == 0 {
		return fmt.Errorf("couldn't update user because of missing device id")
	}

	updateInventory.ID = uint32(*id)
	updateInventory.UserID = uint32(*userID)

	return nil
}
