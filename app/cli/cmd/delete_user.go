package cmd

import (
	"flag"
	"fmt"
)

// DeleteUser struct to handle flags
type DeleteUser struct {
	Cmd *flag.FlagSet
	ID  uint32
}

var deleteUserID *uint64

// NewDeleteUser creates CreateUser flag instance
func NewDeleteUser() *DeleteUser {
	var deleteUser DeleteUser

	deleteUser.Cmd = flag.NewFlagSet("delete-user", flag.ExitOnError)
	deleteUserID = deleteUser.Cmd.Uint64("id", 0, "user id")

	return &deleteUser
}

// Parse subcommand flags
func (deleteUser *DeleteUser) Parse(args []string) error {
	err := deleteUser.Cmd.Parse(args)
	if err != nil {
		return fmt.Errorf("couldn't parse flags for 'update-inventory' command: %v", err)
	}

	if *deleteUserID == 0 {
		return fmt.Errorf("user id must be specified")
	}

	deleteUser.ID = uint32(*deleteUserID)

	return nil
}
