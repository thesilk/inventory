package cmd

import (
	"flag"
	"fmt"
)

//UpdateUser struct to handle flags
type UpdateUser struct {
	Cmd                *flag.FlagSet
	ID                 uint32
	UserName, UserRole string
}

var (
	updateUserID                   *uint64
	updateUserName, updateUserRole *string
)

// NewUpdateUser creates UpdateUser flag instance
func NewUpdateUser() *UpdateUser {
	var updateUser UpdateUser

	updateUser.Cmd = flag.NewFlagSet("update-user", flag.ExitOnError)
	updateUserID = updateUser.Cmd.Uint64("id", 0, "user id")
	updateUserName = updateUser.Cmd.String("user-name", "", "user name")
	updateUserRole = updateUser.Cmd.String("user-role", "", "user role")

	return &updateUser
}

// Parse subcommand flags
func (updateUser *UpdateUser) Parse(args []string) error {
	err := updateUser.Cmd.Parse(args)
	if err != nil {
		return fmt.Errorf("couldn't parse flags for 'update-user' command: %v", err)
	}

	if *updateUserID == 0 {
		return fmt.Errorf("couldn't update user because of missing user id")
	}

	updateUser.ID = uint32(*updateUserID)
	updateUser.UserName = *updateUserName
	updateUser.UserRole = *updateUserRole

	return nil
}
