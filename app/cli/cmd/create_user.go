package cmd

import (
	"flag"
	"fmt"
)

// CreateUser struct to handle flags
type CreateUser struct {
	Cmd                *flag.FlagSet
	UserName, UserRole string
}

var userName, userRole *string

// NewCreateUser creates CreateUser flag instance
func NewCreateUser() *CreateUser {
	var createUser CreateUser

	createUser.Cmd = flag.NewFlagSet("create-user", flag.ExitOnError)
	userName = createUser.Cmd.String("user-name", "", "user name")
	userRole = createUser.Cmd.String("user-role", "", "user role")

	return &createUser
}

// Parse subcommand flags
func (createUser *CreateUser) Parse(args []string) error {
	err := createUser.Cmd.Parse(args)
	if err != nil {
		return fmt.Errorf("couldn't parse flags for 'update-inventory' command: %v", err)
	}

	if *userName == "" {
		return fmt.Errorf("user-name must be specified")
	}

	createUser.UserName = *userName
	createUser.UserRole = *userRole

	return nil
}
