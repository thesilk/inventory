package cmd

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeleteUserCommand(t *testing.T) {
	t.Run("creating new struct", testDeleteUserCommand)
	t.Run("parse with given arguments", testDeleteUserParsing)
	t.Run("parse with no given id", testDeleteUserParsingNoIDGiven)
}

func testDeleteUserCommand(t *testing.T) {
	deleteUser := NewDeleteUser()

	assert.Equal(t, uint32(0), deleteUser.ID)
}

func testDeleteUserParsing(t *testing.T) {
	deleteUser := NewDeleteUser()

	err := deleteUser.Parse([]string{"--id", "5"})

	assert.Nil(t, err)
	assert.Equal(t, uint32(5), deleteUser.ID)

}

func testDeleteUserParsingNoIDGiven(t *testing.T) {
	deleteUser := NewDeleteUser()

	err := deleteUser.Parse([]string{})

	assert.Equal(t, fmt.Errorf("user id must be specified"), err)
}
