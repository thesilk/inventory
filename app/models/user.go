package models

// User struct
type User struct {
	ID   uint32 `json:"id"`
	Name string `json:"name"`
	Role string `json:"role"`
}
