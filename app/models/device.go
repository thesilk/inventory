package models

// Device struct
type Device struct {
	ID            uint32 `json:"id"`
	Name          string `json:"name"`
	HostName      string `json:"host_name"`
	IP            string `json:"ip"`
	Port          uint16 `json:"port"`
	Version       string `json:"version"`
	LongTermUsage bool   `json:"long_term_usage"`
}
