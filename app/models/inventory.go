package models

// Inventory struct
type Inventory struct {
	ID            uint32 `json:"id"`
	Name          string `json:"name"`
	HostName      string `json:"host_name"`
	IP            string `json:"ip"`
	Port          uint16 `json:"port"`
	Version       string `json:"version"`
	UserName      string `json:"user_name"`
	UserRole      string `json:"user_role"`
	UpdateTime    uint32 `json:"update_time"`
	LongTermUsage bool   `json:"long_term_usage"`
}
