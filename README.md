# Inventory

The inventory service a a backend REST API service to manage devices in a test lab.
So a user can assign their to a special device. After the usage the user can unassign their and
another user can take this device. If the user will forget to unassign their the service will
unassign all users each night at 0:00 (UTC). Only if a device was set in a long term usage mode
the unassignment will be skipped for this device.

This backend service will be shipped with a cli tool. But also a usage of a frontend is possible.

## Installation

You can install the server component with

```bash
$ dpkg -i inventory-*.deb
```

The deb files can be downloaded in the CI/CD section (package jobs).

## Development

### Testing

All tests can be executed on your host

```bash
$ make test
OR
$ make coverage
```

or in a docker container:

```bash
$ make docker-test
OR
$ make docker-coverage
```

This project uses [golang-ci](https://golangci-lint.run/) for the linting. This is in the docker
container provided but it has to be installed on your localhost if you want to develop it locally.

### Packaging

This project supports debian based linux systems like Debian or Ubuntu. With

```bash
$ make package
or
$ make docker-package
```
a deb file will be generated. This deb file can be installed with

```bash
$ dpkg -i inventory-*.deb
```

### Documentation

This project uses [API Blueprint](https://apiblueprint.org/) for documentation. You can generate
the documentation with

```bash
$ make doc
or
$ make docker-doc
```

The documentation will be deployed on [Gitlab Pages](https://thesilk.gitlab.io/inventory).
