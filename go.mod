module gitlab.com/thesilk/inventory

go 1.14

require (
	github.com/go-openapi/strfmt v0.19.11 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.2.0
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible
	github.com/robfig/cron/v3 v3.0.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/thesilk/privlib v0.2.0
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
	gopkg.in/testfixtures.v2 v2.6.0
	gopkg.in/yaml.v2 v2.3.0
)
